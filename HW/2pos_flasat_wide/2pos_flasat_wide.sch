EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_1
U 1 1 5F4F8029
P 2000 2075
F 0 "H1_1" H 2025 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 2050 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 2000 3466 60  0001 C CNN
F 3 "" H 2000 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 2000 2075 50  0001 C CNN "MFPN"
	1    2000 2075
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_1
U 1 1 5F4F9D6A
P 3350 2075
F 0 "H2_1" H 3375 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 3400 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 3350 3466 60  0001 C CNN
F 3 "" H 3350 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 3350 2075 50  0001 C CNN "MFPN"
	1    3350 2075
	1    0    0    -1  
$EndComp
Text GLabel 1725 875  0    50   BiDi ~ 0
H1_01
Text GLabel 2375 875  2    50   BiDi ~ 0
H1_02
Text GLabel 1725 975  0    50   BiDi ~ 0
H1_03
Text GLabel 2375 975  2    50   BiDi ~ 0
H1_04
Wire Wire Line
	1725 875  1800 875 
Wire Wire Line
	1725 975  1800 975 
Text GLabel 1725 1075 0    50   BiDi ~ 0
H1_05
Text GLabel 1725 1175 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	1725 1075 1800 1075
Wire Wire Line
	1725 1175 1800 1175
Text GLabel 1725 1275 0    50   BiDi ~ 0
H1_09
Text GLabel 1725 1375 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	1725 1275 1800 1275
Wire Wire Line
	1725 1375 1800 1375
Text GLabel 1725 1475 0    50   BiDi ~ 0
H1_13
Text GLabel 1725 1575 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	1725 1475 1800 1475
Wire Wire Line
	1725 1575 1800 1575
Text GLabel 1725 1675 0    50   BiDi ~ 0
H1_17
Text GLabel 1725 1775 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	1725 1675 1800 1675
Wire Wire Line
	1725 1775 1800 1775
Text GLabel 1725 1875 0    50   BiDi ~ 0
H1_21
Text GLabel 1725 1975 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	1725 1875 1800 1875
Wire Wire Line
	1725 1975 1800 1975
Text GLabel 1725 2175 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	1725 2075 1800 2075
Wire Wire Line
	1725 2175 1800 2175
Text GLabel 1725 2275 0    50   BiDi ~ 0
H1_29
Text GLabel 1725 2375 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	1725 2275 1800 2275
Wire Wire Line
	1725 2375 1800 2375
Text GLabel 1725 2475 0    50   BiDi ~ 0
H1_33
Text GLabel 1725 2575 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	1725 2475 1800 2475
Wire Wire Line
	1725 2575 1800 2575
Text GLabel 1725 2675 0    50   BiDi ~ 0
H1_37
Text GLabel 1725 2775 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	1725 2675 1800 2675
Wire Wire Line
	1725 2775 1800 2775
Text GLabel 1725 2875 0    50   BiDi ~ 0
H1_41
Text GLabel 1725 2975 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	1725 2875 1800 2875
Wire Wire Line
	1725 2975 1800 2975
Text GLabel 1725 3075 0    50   BiDi ~ 0
H1_45
Text GLabel 1725 3175 0    50   BiDi ~ 10
LU_1
Wire Wire Line
	1725 3075 1800 3075
Wire Wire Line
	1725 3175 1800 3175
Text GLabel 1725 3275 0    50   BiDi ~ 10
LU_2
Text GLabel 1725 3375 0    50   BiDi ~ 10
LU_3
Wire Wire Line
	1725 3275 1800 3275
Wire Wire Line
	1725 3375 1800 3375
Wire Wire Line
	2300 875  2375 875 
Wire Wire Line
	2300 975  2375 975 
Text GLabel 2375 1075 2    50   BiDi ~ 0
H1_06
Text GLabel 2375 1175 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	2300 1075 2375 1075
Wire Wire Line
	2300 1175 2375 1175
Text GLabel 2375 1275 2    50   BiDi ~ 0
H1_10
Text GLabel 2375 1375 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	2300 1275 2375 1275
Wire Wire Line
	2300 1375 2375 1375
Text GLabel 2375 1475 2    50   BiDi ~ 0
H1_14
Text GLabel 2375 1575 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	2300 1475 2375 1475
Wire Wire Line
	2300 1575 2375 1575
Text GLabel 2375 1675 2    50   BiDi ~ 0
H1_18
Text GLabel 2375 1775 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	2300 1675 2375 1675
Wire Wire Line
	2300 1775 2375 1775
Text GLabel 2375 1875 2    50   BiDi ~ 0
H1_22
Text GLabel 2375 1975 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	2300 1875 2375 1875
Wire Wire Line
	2300 1975 2375 1975
Text GLabel 2375 2175 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	2300 2075 2375 2075
Wire Wire Line
	2300 2175 2375 2175
Text GLabel 2375 2275 2    50   BiDi ~ 0
H1_30
Text GLabel 2375 2375 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	2300 2275 2375 2275
Wire Wire Line
	2300 2375 2375 2375
Text GLabel 2375 2475 2    50   BiDi ~ 0
H1_34
Text GLabel 2375 2575 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	2300 2475 2375 2475
Wire Wire Line
	2300 2575 2375 2575
Text GLabel 2375 2675 2    50   BiDi ~ 0
H1_38
Text GLabel 2375 2775 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	2300 2675 2375 2675
Wire Wire Line
	2300 2775 2375 2775
Text GLabel 2375 2875 2    50   BiDi ~ 0
H1_42
Text GLabel 2375 2975 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	2300 2875 2375 2875
Wire Wire Line
	2300 2975 2375 2975
Text GLabel 2375 3075 2    50   BiDi ~ 0
H1_46
Text GLabel 2375 3175 2    50   BiDi ~ 10
LU_4
Wire Wire Line
	2300 3075 2375 3075
Wire Wire Line
	2300 3175 2375 3175
Text GLabel 2375 3275 2    50   BiDi ~ 10
LU_5
Text GLabel 2375 3375 2    50   BiDi ~ 10
LU_6
Wire Wire Line
	2300 3275 2375 3275
Wire Wire Line
	2300 3375 2375 3375
Text GLabel 3075 875  0    50   BiDi ~ 0
H2_01
Text GLabel 3075 975  0    50   BiDi ~ 0
H2_03
Wire Wire Line
	3075 875  3150 875 
Wire Wire Line
	3075 975  3150 975 
Text GLabel 3075 1075 0    50   BiDi ~ 0
H2_05
Text GLabel 3075 1175 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	3075 1075 3150 1075
Wire Wire Line
	3075 1175 3150 1175
Text GLabel 3075 1275 0    50   BiDi ~ 0
H2_09
Text GLabel 3075 1375 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	3075 1275 3150 1275
Wire Wire Line
	3075 1375 3150 1375
Text GLabel 3075 1475 0    50   BiDi ~ 0
H2_13
Text GLabel 3075 1575 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	3075 1475 3150 1475
Wire Wire Line
	3075 1575 3150 1575
Text GLabel 3075 1675 0    50   BiDi ~ 0
H2_17
Text GLabel 3075 1775 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	3075 1675 3150 1675
Wire Wire Line
	3075 1775 3150 1775
Text GLabel 3075 1875 0    50   BiDi ~ 0
H2_21
Text GLabel 3075 1975 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	3075 1875 3150 1875
Wire Wire Line
	3075 1975 3150 1975
Wire Wire Line
	3075 2075 3150 2075
Wire Wire Line
	3075 2175 3150 2175
Wire Wire Line
	3075 2275 3150 2275
Wire Wire Line
	3075 2375 3150 2375
Text GLabel 3075 2475 0    50   BiDi ~ 0
H2_33
Text GLabel 3075 2575 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	3075 2475 3150 2475
Wire Wire Line
	3075 2575 3150 2575
Text GLabel 3075 2675 0    50   BiDi ~ 0
H2_37
Text GLabel 3075 2775 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	3075 2675 3150 2675
Wire Wire Line
	3075 2775 3150 2775
Text GLabel 3075 2875 0    50   BiDi ~ 0
H2_41
Text GLabel 3075 2975 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	3075 2875 3150 2875
Wire Wire Line
	3075 2975 3150 2975
Text GLabel 3075 3175 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	3075 3075 3150 3075
Wire Wire Line
	3075 3175 3150 3175
Text GLabel 3075 3275 0    50   BiDi ~ 0
H2_49
Text GLabel 3075 3375 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	3075 3275 3150 3275
Wire Wire Line
	3075 3375 3150 3375
Text GLabel 3725 875  2    50   BiDi ~ 0
H2_02
Text GLabel 3725 975  2    50   BiDi ~ 0
H2_04
Wire Wire Line
	3650 875  3725 875 
Wire Wire Line
	3650 975  3725 975 
Text GLabel 3725 1075 2    50   BiDi ~ 0
H2_06
Text GLabel 3725 1175 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	3650 1075 3725 1075
Wire Wire Line
	3650 1175 3725 1175
Text GLabel 3725 1275 2    50   BiDi ~ 0
H2_10
Text GLabel 3725 1375 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	3650 1275 3725 1275
Wire Wire Line
	3650 1375 3725 1375
Text GLabel 3725 1475 2    50   BiDi ~ 0
H2_14
Text GLabel 3725 1575 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	3650 1475 3725 1475
Wire Wire Line
	3650 1575 3725 1575
Text GLabel 3725 1675 2    50   BiDi ~ 0
H2_18
Text GLabel 3725 1775 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	3650 1675 3725 1675
Wire Wire Line
	3650 1775 3725 1775
Text GLabel 3725 1875 2    50   BiDi ~ 0
H2_22
Text GLabel 3725 1975 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	3650 1875 3725 1875
Wire Wire Line
	3650 1975 3725 1975
Wire Wire Line
	3650 2075 3725 2075
Wire Wire Line
	3650 2175 3725 2175
Wire Wire Line
	3650 2275 3725 2275
Wire Wire Line
	3650 2375 3725 2375
Text GLabel 3725 2475 2    50   BiDi ~ 0
H2_34
Text GLabel 3725 2575 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	3650 2475 3725 2475
Wire Wire Line
	3650 2575 3725 2575
Text GLabel 3725 2675 2    50   BiDi ~ 0
H2_38
Text GLabel 3725 2775 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	3650 2675 3725 2675
Wire Wire Line
	3650 2775 3725 2775
Text GLabel 3725 2875 2    50   BiDi ~ 0
H2_42
Text GLabel 3725 2975 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	3650 2875 3725 2875
Wire Wire Line
	3650 2975 3725 2975
Text GLabel 3725 3175 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	3650 3075 3725 3075
Wire Wire Line
	3650 3175 3725 3175
Text GLabel 3725 3275 2    50   BiDi ~ 0
H2_50
Text GLabel 3725 3375 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	3650 3275 3725 3275
Wire Wire Line
	3650 3375 3725 3375
$Comp
L Mechanical:MountingHole_Pad M1_1
U 1 1 5F7CE2AE
P 875 975
F 0 "M1_1" H 750 1150 60  0000 L CNN
F 1 "HOLE" V 947 1030 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 875 975 60  0001 C CNN
F 3 "" H 875 975 60  0000 C CNN
	1    875  975 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M3_1
U 1 1 5F7D0057
P 875 1225
F 0 "M3_1" H 800 1425 60  0000 L CNN
F 1 "HOLE" V 947 1280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 875 1225 60  0001 C CNN
F 3 "" H 875 1225 60  0000 C CNN
	1    875  1225
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_6
U 1 1 5FAE6E67
P 2000 5000
F 0 "H1_6" H 2050 6425 60  0000 C CNN
F 1 "HEADER_2x26" H 2050 6325 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 2000 6391 60  0001 C CNN
F 3 "" H 2000 6250 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 2000 5000 50  0001 C CNN "MFPN"
	1    2000 5000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_6
U 1 1 5FAE6E6D
P 3350 5000
F 0 "H2_6" H 3375 6425 60  0000 C CNN
F 1 "HEADER_2x26" H 3400 6325 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 3350 6391 60  0001 C CNN
F 3 "" H 3350 6250 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 3350 5000 50  0001 C CNN "MFPN"
	1    3350 5000
	1    0    0    -1  
$EndComp
Text GLabel 1725 3800 0    50   BiDi ~ 0
H1_01
Text GLabel 2375 3800 2    50   BiDi ~ 0
H1_02
Text GLabel 1725 3900 0    50   BiDi ~ 0
H1_03
Text GLabel 2375 3900 2    50   BiDi ~ 0
H1_04
Wire Wire Line
	1725 3800 1800 3800
Wire Wire Line
	1725 3900 1800 3900
Text GLabel 1725 4000 0    50   BiDi ~ 0
H1_05
Text GLabel 1725 4100 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	1725 4000 1800 4000
Wire Wire Line
	1725 4100 1800 4100
Text GLabel 1725 4200 0    50   BiDi ~ 0
H1_09
Text GLabel 1725 4300 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	1725 4200 1800 4200
Wire Wire Line
	1725 4300 1800 4300
Text GLabel 1725 4400 0    50   BiDi ~ 0
H1_13
Text GLabel 1725 4500 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	1725 4400 1800 4400
Wire Wire Line
	1725 4500 1800 4500
Text GLabel 1725 4600 0    50   BiDi ~ 0
H1_17
Text GLabel 1725 4700 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	1725 4600 1800 4600
Wire Wire Line
	1725 4700 1800 4700
Text GLabel 1725 4800 0    50   BiDi ~ 0
H1_21
Text GLabel 1725 4900 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	1725 4800 1800 4800
Wire Wire Line
	1725 4900 1800 4900
Text GLabel 1725 5000 0    50   BiDi ~ 0
H1_25
Text GLabel 1725 5100 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	1725 5000 1800 5000
Wire Wire Line
	1725 5100 1800 5100
Text GLabel 1725 5200 0    50   BiDi ~ 0
H1_29
Text GLabel 1725 5300 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	1725 5200 1800 5200
Wire Wire Line
	1725 5300 1800 5300
Text GLabel 1725 5400 0    50   BiDi ~ 0
H1_33
Text GLabel 1725 5500 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	1725 5400 1800 5400
Wire Wire Line
	1725 5500 1800 5500
Text GLabel 1725 5600 0    50   BiDi ~ 0
H1_37
Text GLabel 1725 5700 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	1725 5600 1800 5600
Wire Wire Line
	1725 5700 1800 5700
Text GLabel 1725 5800 0    50   BiDi ~ 0
H1_41
Text GLabel 1725 5900 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	1725 5800 1800 5800
Wire Wire Line
	1725 5900 1800 5900
Text GLabel 1725 6000 0    50   BiDi ~ 0
H1_45
Wire Wire Line
	1725 6000 1800 6000
Wire Wire Line
	1725 6100 1800 6100
Wire Wire Line
	1725 6200 1800 6200
Wire Wire Line
	1725 6300 1800 6300
Wire Wire Line
	2300 3800 2375 3800
Wire Wire Line
	2300 3900 2375 3900
Text GLabel 2375 4000 2    50   BiDi ~ 0
H1_06
Text GLabel 2375 4100 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	2300 4000 2375 4000
Wire Wire Line
	2300 4100 2375 4100
Text GLabel 2375 4200 2    50   BiDi ~ 0
H1_10
Text GLabel 2375 4300 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	2300 4200 2375 4200
Wire Wire Line
	2300 4300 2375 4300
Text GLabel 2375 4400 2    50   BiDi ~ 0
H1_14
Text GLabel 2375 4500 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	2300 4400 2375 4400
Wire Wire Line
	2300 4500 2375 4500
Text GLabel 2375 4600 2    50   BiDi ~ 0
H1_18
Text GLabel 2375 4700 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	2300 4600 2375 4600
Wire Wire Line
	2300 4700 2375 4700
Text GLabel 2375 4800 2    50   BiDi ~ 0
H1_22
Text GLabel 2375 4900 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	2300 4800 2375 4800
Wire Wire Line
	2300 4900 2375 4900
Text GLabel 2375 5000 2    50   BiDi ~ 0
H1_26
Text GLabel 2375 5100 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	2300 5000 2375 5000
Wire Wire Line
	2300 5100 2375 5100
Text GLabel 2375 5200 2    50   BiDi ~ 0
H1_30
Text GLabel 2375 5300 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	2300 5200 2375 5200
Wire Wire Line
	2300 5300 2375 5300
Text GLabel 2375 5400 2    50   BiDi ~ 0
H1_34
Text GLabel 2375 5500 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	2300 5400 2375 5400
Wire Wire Line
	2300 5500 2375 5500
Text GLabel 2375 5600 2    50   BiDi ~ 0
H1_38
Text GLabel 2375 5700 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	2300 5600 2375 5600
Wire Wire Line
	2300 5700 2375 5700
Text GLabel 2375 5800 2    50   BiDi ~ 0
H1_42
Text GLabel 2375 5900 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	2300 5800 2375 5800
Wire Wire Line
	2300 5900 2375 5900
Text GLabel 2375 6000 2    50   BiDi ~ 0
H1_46
Wire Wire Line
	2300 6000 2375 6000
Wire Wire Line
	2300 6100 2375 6100
Wire Wire Line
	2300 6200 2375 6200
Wire Wire Line
	2300 6300 2375 6300
Text GLabel 3075 3800 0    50   BiDi ~ 0
H2_01
Text GLabel 3075 3900 0    50   BiDi ~ 0
H2_03
Wire Wire Line
	3075 3800 3150 3800
Wire Wire Line
	3075 3900 3150 3900
Text GLabel 3075 4000 0    50   BiDi ~ 0
H2_05
Text GLabel 3075 4100 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	3075 4000 3150 4000
Wire Wire Line
	3075 4100 3150 4100
Text GLabel 3075 4200 0    50   BiDi ~ 0
H2_09
Text GLabel 3075 4300 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	3075 4200 3150 4200
Wire Wire Line
	3075 4300 3150 4300
Text GLabel 3075 4400 0    50   BiDi ~ 0
H2_13
Text GLabel 3075 4500 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	3075 4400 3150 4400
Wire Wire Line
	3075 4500 3150 4500
Text GLabel 3075 4600 0    50   BiDi ~ 0
H2_17
Text GLabel 3075 4700 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	3075 4600 3150 4600
Wire Wire Line
	3075 4700 3150 4700
Text GLabel 3075 4800 0    50   BiDi ~ 0
H2_21
Text GLabel 3075 4900 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	3075 4800 3150 4800
Wire Wire Line
	3075 4900 3150 4900
Wire Wire Line
	3075 5000 3150 5000
Wire Wire Line
	3075 5100 3150 5100
Wire Wire Line
	3075 5200 3150 5200
Wire Wire Line
	3075 5300 3150 5300
Text GLabel 3075 5400 0    50   BiDi ~ 0
H2_33
Text GLabel 3075 5500 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	3075 5400 3150 5400
Wire Wire Line
	3075 5500 3150 5500
Text GLabel 3075 5600 0    50   BiDi ~ 0
H2_37
Text GLabel 3075 5700 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	3075 5600 3150 5600
Wire Wire Line
	3075 5700 3150 5700
Text GLabel 3075 5800 0    50   BiDi ~ 0
H2_41
Text GLabel 3075 5900 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	3075 5800 3150 5800
Wire Wire Line
	3075 5900 3150 5900
Text GLabel 3075 6000 0    50   BiDi ~ 10
V_BATT
Text GLabel 3075 6100 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	3075 6000 3150 6000
Wire Wire Line
	3075 6100 3150 6100
Text GLabel 3075 6200 0    50   BiDi ~ 0
H2_49
Text GLabel 3075 6300 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	3075 6200 3150 6200
Wire Wire Line
	3075 6300 3150 6300
Text GLabel 3725 3800 2    50   BiDi ~ 0
H2_02
Text GLabel 3725 3900 2    50   BiDi ~ 0
H2_04
Wire Wire Line
	3650 3800 3725 3800
Wire Wire Line
	3650 3900 3725 3900
Text GLabel 3725 4000 2    50   BiDi ~ 0
H2_06
Text GLabel 3725 4100 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	3650 4000 3725 4000
Wire Wire Line
	3650 4100 3725 4100
Text GLabel 3725 4200 2    50   BiDi ~ 0
H2_10
Text GLabel 3725 4300 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	3650 4200 3725 4200
Wire Wire Line
	3650 4300 3725 4300
Text GLabel 3725 4400 2    50   BiDi ~ 0
H2_14
Text GLabel 3725 4500 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	3650 4400 3725 4400
Wire Wire Line
	3650 4500 3725 4500
Text GLabel 3725 4600 2    50   BiDi ~ 0
H2_18
Text GLabel 3725 4700 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	3650 4600 3725 4600
Wire Wire Line
	3650 4700 3725 4700
Text GLabel 3725 4800 2    50   BiDi ~ 0
H2_22
Text GLabel 3725 4900 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	3650 4800 3725 4800
Wire Wire Line
	3650 4900 3725 4900
Wire Wire Line
	3650 5000 3725 5000
Wire Wire Line
	3650 5100 3725 5100
Wire Wire Line
	3650 5200 3725 5200
Wire Wire Line
	3650 5300 3725 5300
Text GLabel 3725 5400 2    50   BiDi ~ 0
H2_34
Text GLabel 3725 5500 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	3650 5400 3725 5400
Wire Wire Line
	3650 5500 3725 5500
Text GLabel 3725 5600 2    50   BiDi ~ 0
H2_38
Text GLabel 3725 5700 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	3650 5600 3725 5600
Wire Wire Line
	3650 5700 3725 5700
Text GLabel 3725 5800 2    50   BiDi ~ 0
H2_42
Text GLabel 3725 5900 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	3650 5800 3725 5800
Wire Wire Line
	3650 5900 3725 5900
Text GLabel 3725 6000 2    50   BiDi ~ 10
V_BATT
Text GLabel 3725 6100 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	3650 6000 3725 6000
Wire Wire Line
	3650 6100 3725 6100
Text GLabel 3725 6200 2    50   BiDi ~ 0
H2_50
Text GLabel 3725 6300 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	3650 6200 3725 6200
Wire Wire Line
	3650 6300 3725 6300
$Comp
L Mechanical:MountingHole_Pad M1_6
U 1 1 5FAE6F43
P 875 3900
F 0 "M1_6" H 725 4075 60  0000 L CNN
F 1 "HOLE" V 947 3955 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 875 3900 60  0001 C CNN
F 3 "" H 875 3900 60  0000 C CNN
	1    875  3900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M2_6
U 1 1 5FAE6F49
P 1125 3900
F 0 "M2_6" H 1025 4075 60  0000 L CNN
F 1 "HOLE" V 1197 3955 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1125 3900 60  0001 C CNN
F 3 "" H 1125 3900 60  0000 C CNN
	1    1125 3900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M3_6
U 1 1 5FAE6F4F
P 875 4150
F 0 "M3_6" H 825 4325 60  0000 L CNN
F 1 "HOLE" V 947 4205 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 875 4150 60  0001 C CNN
F 3 "" H 875 4150 60  0000 C CNN
	1    875  4150
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M4_6
U 1 1 5FAE6F55
P 1125 4150
F 0 "M4_6" H 950 4325 60  0000 L CNN
F 1 "HOLE" V 1197 4205 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1125 4150 60  0001 C CNN
F 3 "" H 1125 4150 60  0000 C CNN
	1    1125 4150
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M1
U 1 1 60DF08E4
P 975 6825
F 0 "M1" H 900 7000 60  0000 L CNN
F 1 "HOLE" V 1047 6880 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 975 6825 60  0001 C CNN
F 3 "" H 975 6825 60  0000 C CNN
	1    975  6825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M3
U 1 1 60DF08F0
P 975 7225
F 0 "M3" H 925 7400 60  0000 L CNN
F 1 "HOLE" V 1047 7280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 975 7225 60  0001 C CNN
F 3 "" H 975 7225 60  0000 C CNN
	1    975  7225
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M4
U 1 1 60DF08F6
P 1275 7225
F 0 "M4" H 1200 7400 60  0000 L CNN
F 1 "HOLE" V 1347 7280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1275 7225 60  0001 C CNN
F 3 "" H 1275 7225 60  0000 C CNN
	1    1275 7225
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M5
U 1 1 60EA0BBB
P 1275 6825
F 0 "M5" H 1200 7000 60  0000 L CNN
F 1 "HOLE" V 1347 6880 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1275 6825 60  0001 C CNN
F 3 "" H 1275 6825 60  0000 C CNN
	1    1275 6825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M9
U 1 1 613EDADB
P 1600 6825
F 0 "M9" H 1525 7000 60  0000 L CNN
F 1 "HOLE" V 1672 6880 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1600 6825 60  0001 C CNN
F 3 "" H 1600 6825 60  0000 C CNN
	1    1600 6825
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_7
U 1 1 6165C256
P 5450 2075
F 0 "H1_7" H 5500 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 5500 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 5450 3466 60  0001 C CNN
F 3 "" H 5450 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 5450 2075 50  0001 C CNN "MFPN"
	1    5450 2075
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_7
U 1 1 6165C25C
P 6800 2075
F 0 "H2_7" H 6825 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 6850 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 6800 3466 60  0001 C CNN
F 3 "" H 6800 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 6800 2075 50  0001 C CNN "MFPN"
	1    6800 2075
	1    0    0    -1  
$EndComp
Text GLabel 5175 875  0    50   BiDi ~ 0
H1_01
Text GLabel 5825 875  2    50   BiDi ~ 0
H1_02
Text GLabel 5175 975  0    50   BiDi ~ 0
H1_03
Text GLabel 5825 975  2    50   BiDi ~ 0
H1_04
Wire Wire Line
	5175 875  5250 875 
Wire Wire Line
	5175 975  5250 975 
Text GLabel 5175 1075 0    50   BiDi ~ 0
H1_05
Text GLabel 5175 1175 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	5175 1075 5250 1075
Wire Wire Line
	5175 1175 5250 1175
Text GLabel 5175 1275 0    50   BiDi ~ 0
H1_09
Text GLabel 5175 1375 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	5175 1275 5250 1275
Wire Wire Line
	5175 1375 5250 1375
Text GLabel 5175 1475 0    50   BiDi ~ 0
H1_13
Text GLabel 5175 1575 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	5175 1475 5250 1475
Wire Wire Line
	5175 1575 5250 1575
Text GLabel 5175 1675 0    50   BiDi ~ 0
H1_17
Text GLabel 5175 1775 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	5175 1675 5250 1675
Wire Wire Line
	5175 1775 5250 1775
Text GLabel 5175 1875 0    50   BiDi ~ 0
H1_21
Text GLabel 5175 1975 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	5175 1875 5250 1875
Wire Wire Line
	5175 1975 5250 1975
Text GLabel 5175 2075 0    50   BiDi ~ 0
H1_25
Text GLabel 5175 2175 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	5175 2075 5250 2075
Wire Wire Line
	5175 2175 5250 2175
Text GLabel 5175 2275 0    50   BiDi ~ 0
H1_29
Text GLabel 5175 2375 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	5175 2275 5250 2275
Wire Wire Line
	5175 2375 5250 2375
Text GLabel 5175 2475 0    50   BiDi ~ 0
H1_33
Text GLabel 5175 2575 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	5175 2475 5250 2475
Wire Wire Line
	5175 2575 5250 2575
Text GLabel 5175 2675 0    50   BiDi ~ 0
H1_37
Text GLabel 5175 2775 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	5175 2675 5250 2675
Wire Wire Line
	5175 2775 5250 2775
Text GLabel 5175 2875 0    50   BiDi ~ 0
H1_41
Text GLabel 5175 2975 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	5175 2875 5250 2875
Wire Wire Line
	5175 2975 5250 2975
Text GLabel 5175 3075 0    50   BiDi ~ 0
H1_45
Wire Wire Line
	5175 3075 5250 3075
Wire Wire Line
	5175 3175 5250 3175
Wire Wire Line
	5175 3275 5250 3275
Wire Wire Line
	5175 3375 5250 3375
Wire Wire Line
	5750 875  5825 875 
Wire Wire Line
	5750 975  5825 975 
Text GLabel 5825 1075 2    50   BiDi ~ 0
H1_06
Text GLabel 5825 1175 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	5750 1075 5825 1075
Wire Wire Line
	5750 1175 5825 1175
Text GLabel 5825 1275 2    50   BiDi ~ 0
H1_10
Text GLabel 5825 1375 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	5750 1275 5825 1275
Wire Wire Line
	5750 1375 5825 1375
Text GLabel 5825 1475 2    50   BiDi ~ 0
H1_14
Text GLabel 5825 1575 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	5750 1475 5825 1475
Wire Wire Line
	5750 1575 5825 1575
Text GLabel 5825 1675 2    50   BiDi ~ 0
H1_18
Text GLabel 5825 1775 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	5750 1675 5825 1675
Wire Wire Line
	5750 1775 5825 1775
Text GLabel 5825 1875 2    50   BiDi ~ 0
H1_22
Text GLabel 5825 1975 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	5750 1875 5825 1875
Wire Wire Line
	5750 1975 5825 1975
Text GLabel 5825 2075 2    50   BiDi ~ 0
H1_26
Text GLabel 5825 2175 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	5750 2075 5825 2075
Wire Wire Line
	5750 2175 5825 2175
Text GLabel 5825 2275 2    50   BiDi ~ 0
H1_30
Text GLabel 5825 2375 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	5750 2275 5825 2275
Wire Wire Line
	5750 2375 5825 2375
Text GLabel 5825 2475 2    50   BiDi ~ 0
H1_34
Text GLabel 5825 2575 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	5750 2475 5825 2475
Wire Wire Line
	5750 2575 5825 2575
Text GLabel 5825 2675 2    50   BiDi ~ 0
H1_38
Text GLabel 5825 2775 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	5750 2675 5825 2675
Wire Wire Line
	5750 2775 5825 2775
Text GLabel 5825 2875 2    50   BiDi ~ 0
H1_42
Text GLabel 5825 2975 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	5750 2875 5825 2875
Wire Wire Line
	5750 2975 5825 2975
Text GLabel 5825 3075 2    50   BiDi ~ 0
H1_46
Wire Wire Line
	5750 3075 5825 3075
Wire Wire Line
	5750 3175 5825 3175
Wire Wire Line
	5750 3275 5825 3275
Wire Wire Line
	5750 3375 5825 3375
Text GLabel 6525 875  0    50   BiDi ~ 0
H2_01
Text GLabel 6525 975  0    50   BiDi ~ 0
H2_03
Wire Wire Line
	6525 875  6600 875 
Wire Wire Line
	6525 975  6600 975 
Text GLabel 6525 1075 0    50   BiDi ~ 0
H2_05
Text GLabel 6525 1175 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	6525 1075 6600 1075
Wire Wire Line
	6525 1175 6600 1175
Text GLabel 6525 1275 0    50   BiDi ~ 0
H2_09
Text GLabel 6525 1375 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	6525 1275 6600 1275
Wire Wire Line
	6525 1375 6600 1375
Text GLabel 6525 1475 0    50   BiDi ~ 0
H2_13
Text GLabel 6525 1575 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	6525 1475 6600 1475
Wire Wire Line
	6525 1575 6600 1575
Text GLabel 6525 1675 0    50   BiDi ~ 0
H2_17
Text GLabel 6525 1775 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	6525 1675 6600 1675
Wire Wire Line
	6525 1775 6600 1775
Text GLabel 6525 1875 0    50   BiDi ~ 0
H2_21
Text GLabel 6525 1975 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	6525 1875 6600 1875
Wire Wire Line
	6525 1975 6600 1975
Wire Wire Line
	6525 2075 6600 2075
Wire Wire Line
	6525 2175 6600 2175
Wire Wire Line
	6525 2275 6600 2275
Wire Wire Line
	6525 2375 6600 2375
Text GLabel 6525 2475 0    50   BiDi ~ 0
H2_33
Text GLabel 6525 2575 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	6525 2475 6600 2475
Wire Wire Line
	6525 2575 6600 2575
Text GLabel 6525 2675 0    50   BiDi ~ 0
H2_37
Text GLabel 6525 2775 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	6525 2675 6600 2675
Wire Wire Line
	6525 2775 6600 2775
Text GLabel 6525 2875 0    50   BiDi ~ 0
H2_41
Text GLabel 6525 2975 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	6525 2875 6600 2875
Wire Wire Line
	6525 2975 6600 2975
Text GLabel 6525 3075 0    50   BiDi ~ 10
V_BATT
Text GLabel 6525 3175 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	6525 3075 6600 3075
Wire Wire Line
	6525 3175 6600 3175
Text GLabel 6525 3275 0    50   BiDi ~ 0
H2_49
Text GLabel 6525 3375 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	6525 3275 6600 3275
Wire Wire Line
	6525 3375 6600 3375
Text GLabel 7175 875  2    50   BiDi ~ 0
H2_02
Text GLabel 7175 975  2    50   BiDi ~ 0
H2_04
Wire Wire Line
	7100 875  7175 875 
Wire Wire Line
	7100 975  7175 975 
Text GLabel 7175 1075 2    50   BiDi ~ 0
H2_06
Text GLabel 7175 1175 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	7100 1075 7175 1075
Wire Wire Line
	7100 1175 7175 1175
Text GLabel 7175 1275 2    50   BiDi ~ 0
H2_10
Text GLabel 7175 1375 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	7100 1275 7175 1275
Wire Wire Line
	7100 1375 7175 1375
Text GLabel 7175 1475 2    50   BiDi ~ 0
H2_14
Text GLabel 7175 1575 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	7100 1475 7175 1475
Wire Wire Line
	7100 1575 7175 1575
Text GLabel 7175 1675 2    50   BiDi ~ 0
H2_18
Text GLabel 7175 1775 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	7100 1675 7175 1675
Wire Wire Line
	7100 1775 7175 1775
Text GLabel 7175 1875 2    50   BiDi ~ 0
H2_22
Text GLabel 7175 1975 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	7100 1875 7175 1875
Wire Wire Line
	7100 1975 7175 1975
Wire Wire Line
	7100 2075 7175 2075
Wire Wire Line
	7100 2175 7175 2175
Wire Wire Line
	7100 2275 7175 2275
Wire Wire Line
	7100 2375 7175 2375
Text GLabel 7175 2475 2    50   BiDi ~ 0
H2_34
Text GLabel 7175 2575 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	7100 2475 7175 2475
Wire Wire Line
	7100 2575 7175 2575
Text GLabel 7175 2675 2    50   BiDi ~ 0
H2_38
Text GLabel 7175 2775 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	7100 2675 7175 2675
Wire Wire Line
	7100 2775 7175 2775
Text GLabel 7175 2875 2    50   BiDi ~ 0
H2_42
Text GLabel 7175 2975 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	7100 2875 7175 2875
Wire Wire Line
	7100 2975 7175 2975
Text GLabel 7175 3075 2    50   BiDi ~ 10
V_BATT
Text GLabel 7175 3175 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	7100 3075 7175 3075
Wire Wire Line
	7100 3175 7175 3175
Text GLabel 7175 3275 2    50   BiDi ~ 0
H2_50
Text GLabel 7175 3375 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	7100 3275 7175 3275
Wire Wire Line
	7100 3375 7175 3375
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_8
U 1 1 6165C34B
P 5450 5050
F 0 "H1_8" H 5475 6475 60  0000 C CNN
F 1 "HEADER_2x26" H 5500 6375 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 5450 6441 60  0001 C CNN
F 3 "" H 5450 6300 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 5450 5050 50  0001 C CNN "MFPN"
	1    5450 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_8
U 1 1 6165C351
P 6800 5050
F 0 "H2_8" H 6850 6475 60  0000 C CNN
F 1 "HEADER_2x26" H 6850 6375 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 6800 6441 60  0001 C CNN
F 3 "" H 6800 6300 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 6800 5050 50  0001 C CNN "MFPN"
	1    6800 5050
	1    0    0    -1  
$EndComp
Text GLabel 5175 3850 0    50   BiDi ~ 0
H1_01
Text GLabel 5825 3850 2    50   BiDi ~ 0
H1_02
Text GLabel 5175 3950 0    50   BiDi ~ 0
H1_03
Text GLabel 5825 3950 2    50   BiDi ~ 0
H1_04
Wire Wire Line
	5175 3850 5250 3850
Wire Wire Line
	5175 3950 5250 3950
Text GLabel 5175 4050 0    50   BiDi ~ 0
H1_05
Text GLabel 5175 4150 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	5175 4050 5250 4050
Wire Wire Line
	5175 4150 5250 4150
Text GLabel 5175 4250 0    50   BiDi ~ 0
H1_09
Text GLabel 5175 4350 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	5175 4250 5250 4250
Wire Wire Line
	5175 4350 5250 4350
Text GLabel 5175 4450 0    50   BiDi ~ 0
H1_13
Text GLabel 5175 4550 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	5175 4450 5250 4450
Wire Wire Line
	5175 4550 5250 4550
Text GLabel 5175 4650 0    50   BiDi ~ 0
H1_17
Text GLabel 5175 4750 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	5175 4650 5250 4650
Wire Wire Line
	5175 4750 5250 4750
Text GLabel 5175 4850 0    50   BiDi ~ 0
H1_21
Text GLabel 5175 4950 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	5175 4850 5250 4850
Wire Wire Line
	5175 4950 5250 4950
Text GLabel 5175 5050 0    50   BiDi ~ 0
H1_25
Text GLabel 5175 5150 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	5175 5050 5250 5050
Wire Wire Line
	5175 5150 5250 5150
Text GLabel 5175 5250 0    50   BiDi ~ 0
H1_29
Text GLabel 5175 5350 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	5175 5250 5250 5250
Wire Wire Line
	5175 5350 5250 5350
Text GLabel 5175 5450 0    50   BiDi ~ 0
H1_33
Text GLabel 5175 5550 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	5175 5450 5250 5450
Wire Wire Line
	5175 5550 5250 5550
Text GLabel 5175 5650 0    50   BiDi ~ 0
H1_37
Text GLabel 5175 5750 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	5175 5650 5250 5650
Wire Wire Line
	5175 5750 5250 5750
Text GLabel 5175 5850 0    50   BiDi ~ 0
H1_41
Text GLabel 5175 5950 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	5175 5850 5250 5850
Wire Wire Line
	5175 5950 5250 5950
Text GLabel 5175 6050 0    50   BiDi ~ 0
H1_45
Wire Wire Line
	5175 6050 5250 6050
Wire Wire Line
	5175 6150 5250 6150
Wire Wire Line
	5175 6250 5250 6250
Wire Wire Line
	5175 6350 5250 6350
Wire Wire Line
	5750 3850 5825 3850
Wire Wire Line
	5750 3950 5825 3950
Text GLabel 5825 4050 2    50   BiDi ~ 0
H1_06
Text GLabel 5825 4150 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	5750 4050 5825 4050
Wire Wire Line
	5750 4150 5825 4150
Text GLabel 5825 4250 2    50   BiDi ~ 0
H1_10
Text GLabel 5825 4350 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	5750 4250 5825 4250
Wire Wire Line
	5750 4350 5825 4350
Text GLabel 5825 4450 2    50   BiDi ~ 0
H1_14
Text GLabel 5825 4550 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	5750 4450 5825 4450
Wire Wire Line
	5750 4550 5825 4550
Text GLabel 5825 4650 2    50   BiDi ~ 0
H1_18
Text GLabel 5825 4750 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	5750 4650 5825 4650
Wire Wire Line
	5750 4750 5825 4750
Text GLabel 5825 4850 2    50   BiDi ~ 0
H1_22
Text GLabel 5825 4950 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	5750 4850 5825 4850
Wire Wire Line
	5750 4950 5825 4950
Text GLabel 5825 5050 2    50   BiDi ~ 0
H1_26
Text GLabel 5825 5150 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	5750 5050 5825 5050
Wire Wire Line
	5750 5150 5825 5150
Text GLabel 5825 5250 2    50   BiDi ~ 0
H1_30
Text GLabel 5825 5350 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	5750 5250 5825 5250
Wire Wire Line
	5750 5350 5825 5350
Text GLabel 5825 5450 2    50   BiDi ~ 0
H1_34
Text GLabel 5825 5550 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	5750 5450 5825 5450
Wire Wire Line
	5750 5550 5825 5550
Text GLabel 5825 5650 2    50   BiDi ~ 0
H1_38
Text GLabel 5825 5750 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	5750 5650 5825 5650
Wire Wire Line
	5750 5750 5825 5750
Text GLabel 5825 5850 2    50   BiDi ~ 0
H1_42
Text GLabel 5825 5950 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	5750 5850 5825 5850
Wire Wire Line
	5750 5950 5825 5950
Text GLabel 5825 6050 2    50   BiDi ~ 0
H1_46
Wire Wire Line
	5750 6050 5825 6050
Wire Wire Line
	5750 6150 5825 6150
Wire Wire Line
	5750 6250 5825 6250
Wire Wire Line
	5750 6350 5825 6350
Text GLabel 6525 3850 0    50   BiDi ~ 0
H2_01
Text GLabel 6525 3950 0    50   BiDi ~ 0
H2_03
Wire Wire Line
	6525 3850 6600 3850
Wire Wire Line
	6525 3950 6600 3950
Text GLabel 6525 4050 0    50   BiDi ~ 0
H2_05
Text GLabel 6525 4150 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	6525 4050 6600 4050
Wire Wire Line
	6525 4150 6600 4150
Text GLabel 6525 4250 0    50   BiDi ~ 0
H2_09
Text GLabel 6525 4350 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	6525 4250 6600 4250
Wire Wire Line
	6525 4350 6600 4350
Text GLabel 6525 4450 0    50   BiDi ~ 0
H2_13
Text GLabel 6525 4550 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	6525 4450 6600 4450
Wire Wire Line
	6525 4550 6600 4550
Text GLabel 6525 4650 0    50   BiDi ~ 0
H2_17
Text GLabel 6525 4750 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	6525 4650 6600 4650
Wire Wire Line
	6525 4750 6600 4750
Text GLabel 6525 4850 0    50   BiDi ~ 0
H2_21
Text GLabel 6525 4950 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	6525 4850 6600 4850
Wire Wire Line
	6525 4950 6600 4950
Wire Wire Line
	6525 5050 6600 5050
Wire Wire Line
	6525 5150 6600 5150
Wire Wire Line
	6525 5250 6600 5250
Wire Wire Line
	6525 5350 6600 5350
Text GLabel 6525 5450 0    50   BiDi ~ 0
H2_33
Text GLabel 6525 5550 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	6525 5450 6600 5450
Wire Wire Line
	6525 5550 6600 5550
Text GLabel 6525 5650 0    50   BiDi ~ 0
H2_37
Text GLabel 6525 5750 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	6525 5650 6600 5650
Wire Wire Line
	6525 5750 6600 5750
Text GLabel 6525 5850 0    50   BiDi ~ 0
H2_41
Text GLabel 6525 5950 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	6525 5850 6600 5850
Wire Wire Line
	6525 5950 6600 5950
Text GLabel 6525 6050 0    50   BiDi ~ 10
V_BATT
Text GLabel 6525 6150 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	6525 6050 6600 6050
Wire Wire Line
	6525 6150 6600 6150
Text GLabel 6525 6250 0    50   BiDi ~ 0
H2_49
Text GLabel 6525 6350 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	6525 6250 6600 6250
Wire Wire Line
	6525 6350 6600 6350
Text GLabel 7175 3850 2    50   BiDi ~ 0
H2_02
Text GLabel 7175 3950 2    50   BiDi ~ 0
H2_04
Wire Wire Line
	7100 3850 7175 3850
Wire Wire Line
	7100 3950 7175 3950
Text GLabel 7175 4050 2    50   BiDi ~ 0
H2_06
Text GLabel 7175 4150 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	7100 4050 7175 4050
Wire Wire Line
	7100 4150 7175 4150
Text GLabel 7175 4250 2    50   BiDi ~ 0
H2_10
Text GLabel 7175 4350 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	7100 4250 7175 4250
Wire Wire Line
	7100 4350 7175 4350
Text GLabel 7175 4450 2    50   BiDi ~ 0
H2_14
Text GLabel 7175 4550 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	7100 4450 7175 4450
Wire Wire Line
	7100 4550 7175 4550
Text GLabel 7175 4650 2    50   BiDi ~ 0
H2_18
Text GLabel 7175 4750 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	7100 4650 7175 4650
Wire Wire Line
	7100 4750 7175 4750
Text GLabel 7175 4850 2    50   BiDi ~ 0
H2_22
Text GLabel 7175 4950 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	7100 4850 7175 4850
Wire Wire Line
	7100 4950 7175 4950
Wire Wire Line
	7100 5050 7175 5050
Wire Wire Line
	7100 5150 7175 5150
Wire Wire Line
	7100 5250 7175 5250
Wire Wire Line
	7100 5350 7175 5350
Text GLabel 7175 5450 2    50   BiDi ~ 0
H2_34
Text GLabel 7175 5550 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	7100 5450 7175 5450
Wire Wire Line
	7100 5550 7175 5550
Text GLabel 7175 5650 2    50   BiDi ~ 0
H2_38
Text GLabel 7175 5750 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	7100 5650 7175 5650
Wire Wire Line
	7100 5750 7175 5750
Text GLabel 7175 5850 2    50   BiDi ~ 0
H2_42
Text GLabel 7175 5950 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	7100 5850 7175 5850
Wire Wire Line
	7100 5950 7175 5950
Text GLabel 7175 6150 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	7100 6050 7175 6050
Wire Wire Line
	7100 6150 7175 6150
Text GLabel 7175 6250 2    50   BiDi ~ 0
H2_50
Text GLabel 7175 6350 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	7100 6250 7175 6250
Wire Wire Line
	7100 6350 7175 6350
$Comp
L power:GND #PWR0101
U 1 1 633079E1
P 3725 2275
F 0 "#PWR0101" H 3725 2025 50  0001 C CNN
F 1 "GND" V 3730 2147 50  0000 R CNN
F 2 "" H 3725 2275 50  0001 C CNN
F 3 "" H 3725 2275 50  0001 C CNN
	1    3725 2275
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6330974E
P 3075 2275
F 0 "#PWR0102" H 3075 2025 50  0001 C CNN
F 1 "GND" V 3080 2147 50  0000 R CNN
F 2 "" H 3075 2275 50  0001 C CNN
F 3 "" H 3075 2275 50  0001 C CNN
	1    3075 2275
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 6330B21B
P 3725 2375
F 0 "#PWR0103" H 3725 2125 50  0001 C CNN
F 1 "GND" V 3730 2247 50  0000 R CNN
F 2 "" H 3725 2375 50  0001 C CNN
F 3 "" H 3725 2375 50  0001 C CNN
	1    3725 2375
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDA #PWR0104
U 1 1 6330CCF4
P 3075 2375
F 0 "#PWR0104" H 3075 2125 50  0001 C CNN
F 1 "GNDA" V 3080 2248 50  0000 R CNN
F 2 "" H 3075 2375 50  0001 C CNN
F 3 "" H 3075 2375 50  0001 C CNN
	1    3075 2375
	0    1    1    0   
$EndComp
Text GLabel 3725 3075 2    50   BiDi ~ 10
V_BATT
Text GLabel 3075 3075 0    50   BiDi ~ 10
V_BATT
$Comp
L power:GND #PWR0109
U 1 1 635BCF27
P 7175 2275
F 0 "#PWR0109" H 7175 2025 50  0001 C CNN
F 1 "GND" V 7180 2147 50  0000 R CNN
F 2 "" H 7175 2275 50  0001 C CNN
F 3 "" H 7175 2275 50  0001 C CNN
	1    7175 2275
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 635BCF2D
P 7175 2375
F 0 "#PWR0110" H 7175 2125 50  0001 C CNN
F 1 "GND" V 7180 2247 50  0000 R CNN
F 2 "" H 7175 2375 50  0001 C CNN
F 3 "" H 7175 2375 50  0001 C CNN
	1    7175 2375
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 636E1A63
P 7175 5250
F 0 "#PWR0111" H 7175 5000 50  0001 C CNN
F 1 "GND" V 7180 5122 50  0000 R CNN
F 2 "" H 7175 5250 50  0001 C CNN
F 3 "" H 7175 5250 50  0001 C CNN
	1    7175 5250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 636E1A69
P 7175 5350
F 0 "#PWR0112" H 7175 5100 50  0001 C CNN
F 1 "GND" V 7180 5222 50  0000 R CNN
F 2 "" H 7175 5350 50  0001 C CNN
F 3 "" H 7175 5350 50  0001 C CNN
	1    7175 5350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 63807367
P 3725 5200
F 0 "#PWR0113" H 3725 4950 50  0001 C CNN
F 1 "GND" V 3730 5072 50  0000 R CNN
F 2 "" H 3725 5200 50  0001 C CNN
F 3 "" H 3725 5200 50  0001 C CNN
	1    3725 5200
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 6380736D
P 3725 5300
F 0 "#PWR0114" H 3725 5050 50  0001 C CNN
F 1 "GND" V 3730 5172 50  0000 R CNN
F 2 "" H 3725 5300 50  0001 C CNN
F 3 "" H 3725 5300 50  0001 C CNN
	1    3725 5300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 63EF2FBF
P 3075 5200
F 0 "#PWR0123" H 3075 4950 50  0001 C CNN
F 1 "GND" V 3080 5072 50  0000 R CNN
F 2 "" H 3075 5200 50  0001 C CNN
F 3 "" H 3075 5200 50  0001 C CNN
	1    3075 5200
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0124
U 1 1 63EF2FC5
P 3075 5300
F 0 "#PWR0124" H 3075 5050 50  0001 C CNN
F 1 "GNDA" V 3080 5173 50  0000 R CNN
F 2 "" H 3075 5300 50  0001 C CNN
F 3 "" H 3075 5300 50  0001 C CNN
	1    3075 5300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 6401877B
P 6525 5250
F 0 "#PWR0125" H 6525 5000 50  0001 C CNN
F 1 "GND" V 6530 5122 50  0000 R CNN
F 2 "" H 6525 5250 50  0001 C CNN
F 3 "" H 6525 5250 50  0001 C CNN
	1    6525 5250
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0126
U 1 1 64018781
P 6525 5350
F 0 "#PWR0126" H 6525 5100 50  0001 C CNN
F 1 "GNDA" V 6530 5223 50  0000 R CNN
F 2 "" H 6525 5350 50  0001 C CNN
F 3 "" H 6525 5350 50  0001 C CNN
	1    6525 5350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 6413C6F1
P 6525 2275
F 0 "#PWR0127" H 6525 2025 50  0001 C CNN
F 1 "GND" V 6530 2147 50  0000 R CNN
F 2 "" H 6525 2275 50  0001 C CNN
F 3 "" H 6525 2275 50  0001 C CNN
	1    6525 2275
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0128
U 1 1 6413C6F7
P 6525 2375
F 0 "#PWR0128" H 6525 2125 50  0001 C CNN
F 1 "GNDA" V 6530 2248 50  0000 R CNN
F 2 "" H 6525 2375 50  0001 C CNN
F 3 "" H 6525 2375 50  0001 C CNN
	1    6525 2375
	0    1    1    0   
$EndComp
Text GLabel 1725 6100 0    50   BiDi ~ 10
LU_1
Text GLabel 1725 6200 0    50   BiDi ~ 10
LU_2
Text GLabel 1725 6300 0    50   BiDi ~ 10
LU_3
Text GLabel 5175 3175 0    50   BiDi ~ 10
LU_1
Text GLabel 5175 3275 0    50   BiDi ~ 10
LU_2
Text GLabel 5175 3375 0    50   BiDi ~ 10
LU_3
Text GLabel 5825 3175 2    50   BiDi ~ 10
LU_4
Text GLabel 5825 3275 2    50   BiDi ~ 10
LU_5
Text GLabel 5825 3375 2    50   BiDi ~ 10
LU_6
Text GLabel 5825 6150 2    50   BiDi ~ 10
LU_4
Text GLabel 5825 6250 2    50   BiDi ~ 10
LU_5
Text GLabel 5825 6350 2    50   BiDi ~ 10
LU_6
Text GLabel 2375 6100 2    50   BiDi ~ 10
LU_4
Text GLabel 2375 6200 2    50   BiDi ~ 10
LU_5
Text GLabel 2375 6300 2    50   BiDi ~ 10
LU_6
Text GLabel 5175 6150 0    50   BiDi ~ 10
LU_1
Text GLabel 5175 6250 0    50   BiDi ~ 10
LU_2
Text GLabel 5175 6350 0    50   BiDi ~ 10
LU_3
Text GLabel 2375 2075 2    50   BiDi ~ 0
H1_26
Text GLabel 1725 2075 0    50   BiDi ~ 0
H1_25
$Comp
L power:+5V #PWR0133
U 1 1 66771852
P 3075 2075
F 0 "#PWR0133" H 3075 1925 50  0001 C CNN
F 1 "+5V" V 3090 2203 50  0000 L CNN
F 2 "" H 3075 2075 50  0001 C CNN
F 3 "" H 3075 2075 50  0001 C CNN
	1    3075 2075
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0134
U 1 1 667730FB
P 3725 2075
F 0 "#PWR0134" H 3725 1925 50  0001 C CNN
F 1 "+5V" V 3740 2203 50  0000 L CNN
F 2 "" H 3725 2075 50  0001 C CNN
F 3 "" H 3725 2075 50  0001 C CNN
	1    3725 2075
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0135
U 1 1 6679F6CF
P 3075 2175
F 0 "#PWR0135" H 3075 2025 50  0001 C CNN
F 1 "+3V3" V 3090 2303 50  0000 L CNN
F 2 "" H 3075 2175 50  0001 C CNN
F 3 "" H 3075 2175 50  0001 C CNN
	1    3075 2175
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0136
U 1 1 667A11B2
P 3725 2175
F 0 "#PWR0136" H 3725 2025 50  0001 C CNN
F 1 "+3V3" V 3740 2303 50  0000 L CNN
F 2 "" H 3725 2175 50  0001 C CNN
F 3 "" H 3725 2175 50  0001 C CNN
	1    3725 2175
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0141
U 1 1 669F171A
P 6525 2075
F 0 "#PWR0141" H 6525 1925 50  0001 C CNN
F 1 "+5V" V 6540 2203 50  0000 L CNN
F 2 "" H 6525 2075 50  0001 C CNN
F 3 "" H 6525 2075 50  0001 C CNN
	1    6525 2075
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0142
U 1 1 669F1720
P 6525 2175
F 0 "#PWR0142" H 6525 2025 50  0001 C CNN
F 1 "+3V3" V 6540 2303 50  0000 L CNN
F 2 "" H 6525 2175 50  0001 C CNN
F 3 "" H 6525 2175 50  0001 C CNN
	1    6525 2175
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0143
U 1 1 66C37C5C
P 6525 5050
F 0 "#PWR0143" H 6525 4900 50  0001 C CNN
F 1 "+5V" V 6540 5178 50  0000 L CNN
F 2 "" H 6525 5050 50  0001 C CNN
F 3 "" H 6525 5050 50  0001 C CNN
	1    6525 5050
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0144
U 1 1 66C37C62
P 6525 5150
F 0 "#PWR0144" H 6525 5000 50  0001 C CNN
F 1 "+3V3" V 6540 5278 50  0000 L CNN
F 2 "" H 6525 5150 50  0001 C CNN
F 3 "" H 6525 5150 50  0001 C CNN
	1    6525 5150
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0145
U 1 1 66D5DD0E
P 3075 5000
F 0 "#PWR0145" H 3075 4850 50  0001 C CNN
F 1 "+5V" V 3090 5128 50  0000 L CNN
F 2 "" H 3075 5000 50  0001 C CNN
F 3 "" H 3075 5000 50  0001 C CNN
	1    3075 5000
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0146
U 1 1 66D5DD14
P 3075 5100
F 0 "#PWR0146" H 3075 4950 50  0001 C CNN
F 1 "+3V3" V 3090 5228 50  0000 L CNN
F 2 "" H 3075 5100 50  0001 C CNN
F 3 "" H 3075 5100 50  0001 C CNN
	1    3075 5100
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0155
U 1 1 6743F890
P 3725 5000
F 0 "#PWR0155" H 3725 4850 50  0001 C CNN
F 1 "+5V" V 3740 5128 50  0000 L CNN
F 2 "" H 3725 5000 50  0001 C CNN
F 3 "" H 3725 5000 50  0001 C CNN
	1    3725 5000
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0156
U 1 1 6743F896
P 3725 5100
F 0 "#PWR0156" H 3725 4950 50  0001 C CNN
F 1 "+3V3" V 3740 5228 50  0000 L CNN
F 2 "" H 3725 5100 50  0001 C CNN
F 3 "" H 3725 5100 50  0001 C CNN
	1    3725 5100
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0157
U 1 1 67564566
P 7175 5050
F 0 "#PWR0157" H 7175 4900 50  0001 C CNN
F 1 "+5V" V 7190 5178 50  0000 L CNN
F 2 "" H 7175 5050 50  0001 C CNN
F 3 "" H 7175 5050 50  0001 C CNN
	1    7175 5050
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0158
U 1 1 6756456C
P 7175 5150
F 0 "#PWR0158" H 7175 5000 50  0001 C CNN
F 1 "+3V3" V 7190 5278 50  0000 L CNN
F 2 "" H 7175 5150 50  0001 C CNN
F 3 "" H 7175 5150 50  0001 C CNN
	1    7175 5150
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0159
U 1 1 67689CF0
P 7175 2075
F 0 "#PWR0159" H 7175 1925 50  0001 C CNN
F 1 "+5V" V 7190 2203 50  0000 L CNN
F 2 "" H 7175 2075 50  0001 C CNN
F 3 "" H 7175 2075 50  0001 C CNN
	1    7175 2075
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0160
U 1 1 67689CF6
P 7175 2175
F 0 "#PWR0160" H 7175 2025 50  0001 C CNN
F 1 "+3V3" V 7190 2303 50  0000 L CNN
F 2 "" H 7175 2175 50  0001 C CNN
F 3 "" H 7175 2175 50  0001 C CNN
	1    7175 2175
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0165
U 1 1 5F8A509D
P 1000 1350
F 0 "#PWR0165" H 1000 1100 50  0001 C CNN
F 1 "GND" H 1005 1177 50  0000 C CNN
F 2 "" H 1000 1350 50  0001 C CNN
F 3 "" H 1000 1350 50  0001 C CNN
	1    1000 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	875  1075 1000 1075
Wire Wire Line
	875  1125 1000 1125
Wire Wire Line
	1000 1075 1000 1125
Connection ~ 1000 1075
Wire Wire Line
	1000 1075 1125 1075
Wire Wire Line
	1000 1125 1125 1125
Wire Wire Line
	875  4000 1000 4000
Wire Wire Line
	875  4050 1000 4050
Wire Wire Line
	1000 4000 1000 4050
Connection ~ 1000 4000
Wire Wire Line
	1000 4000 1125 4000
Wire Wire Line
	1000 4050 1125 4050
$Comp
L power:GND #PWR0166
U 1 1 6157BB0F
P 1825 7075
F 0 "#PWR0166" H 1825 6825 50  0001 C CNN
F 1 "GND" H 1830 6902 50  0000 C CNN
F 2 "" H 1825 7075 50  0001 C CNN
F 3 "" H 1825 7075 50  0001 C CNN
	1    1825 7075
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0169
U 1 1 615906F8
P 1000 4300
F 0 "#PWR0169" H 1000 4050 50  0001 C CNN
F 1 "GND" H 1005 4127 50  0000 C CNN
F 2 "" H 1000 4300 50  0001 C CNN
F 3 "" H 1000 4300 50  0001 C CNN
	1    1000 4300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M4_1
U 1 1 5F7D0737
P 1125 1225
F 0 "M4_1" H 950 1425 60  0000 L CNN
F 1 "HOLE" V 1197 1280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1125 1225 60  0001 C CNN
F 3 "" H 1125 1225 60  0000 C CNN
	1    1125 1225
	-1   0    0    1   
$EndComp
Connection ~ 1000 1125
Wire Wire Line
	1000 1125 1000 1350
$Comp
L Mechanical:MountingHole_Pad M2_1
U 1 1 5F7CF7DD
P 1125 975
F 0 "M2_1" H 1025 1150 60  0000 L CNN
F 1 "HOLE" V 1197 1030 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1125 975 60  0001 C CNN
F 3 "" H 1125 975 60  0000 C CNN
	1    1125 975 
	1    0    0    -1  
$EndComp
Connection ~ 1000 4050
Wire Wire Line
	1000 4050 1000 4300
$Comp
L Mechanical:MountingHole_Pad M8
U 1 1 613338EA
P 1600 7225
F 0 "M8" H 1525 7400 60  0000 L CNN
F 1 "HOLE" V 1672 7280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1600 7225 60  0001 C CNN
F 3 "" H 1600 7225 60  0000 C CNN
	1    1600 7225
	-1   0    0    1   
$EndComp
Wire Wire Line
	1825 7025 1825 7075
Wire Wire Line
	1275 6925 1275 7025
Wire Wire Line
	1275 7025 1600 7025
Wire Wire Line
	1275 6925 1600 6925
Wire Wire Line
	975  6925 1275 6925
Connection ~ 1275 6925
NoConn ~ 1275 7125
Wire Wire Line
	975  7125 975  7025
Wire Wire Line
	975  7025 1275 7025
Connection ~ 1275 7025
Wire Wire Line
	1600 7125 1600 7025
Connection ~ 1600 7025
Wire Wire Line
	1600 7025 1825 7025
Text GLabel 7175 6050 2    50   BiDi ~ 10
V_BATT
$EndSCHEMATC
