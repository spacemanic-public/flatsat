EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_1
U 1 1 5F4F8029
P 2000 2075
F 0 "H1_1" H 2025 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 2050 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 2000 3466 60  0001 C CNN
F 3 "" H 2000 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 2000 2075 50  0001 C CNN "MFPN"
	1    2000 2075
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_1
U 1 1 5F4F9D6A
P 3350 2075
F 0 "H2_1" H 3375 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 3400 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 3350 3466 60  0001 C CNN
F 3 "" H 3350 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 3350 2075 50  0001 C CNN "MFPN"
	1    3350 2075
	1    0    0    -1  
$EndComp
Text GLabel 1725 875  0    50   BiDi ~ 0
H1_01
Text GLabel 2375 875  2    50   BiDi ~ 0
H1_02
Text GLabel 1725 975  0    50   BiDi ~ 0
H1_03
Text GLabel 2375 975  2    50   BiDi ~ 0
H1_04
Wire Wire Line
	1725 875  1800 875 
Wire Wire Line
	1725 975  1800 975 
Text GLabel 1725 1075 0    50   BiDi ~ 0
H1_05
Text GLabel 1725 1175 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	1725 1075 1800 1075
Wire Wire Line
	1725 1175 1800 1175
Text GLabel 1725 1275 0    50   BiDi ~ 0
H1_09
Text GLabel 1725 1375 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	1725 1275 1800 1275
Wire Wire Line
	1725 1375 1800 1375
Text GLabel 1725 1475 0    50   BiDi ~ 0
H1_13
Text GLabel 1725 1575 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	1725 1475 1800 1475
Wire Wire Line
	1725 1575 1800 1575
Text GLabel 1725 1675 0    50   BiDi ~ 0
H1_17
Text GLabel 1725 1775 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	1725 1675 1800 1675
Wire Wire Line
	1725 1775 1800 1775
Text GLabel 1725 1875 0    50   BiDi ~ 0
H1_21
Text GLabel 1725 1975 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	1725 1875 1800 1875
Wire Wire Line
	1725 1975 1800 1975
Text GLabel 1725 2175 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	1725 2075 1800 2075
Wire Wire Line
	1725 2175 1800 2175
Text GLabel 1725 2275 0    50   BiDi ~ 0
H1_29
Text GLabel 1725 2375 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	1725 2275 1800 2275
Wire Wire Line
	1725 2375 1800 2375
Text GLabel 1725 2475 0    50   BiDi ~ 0
H1_33
Text GLabel 1725 2575 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	1725 2475 1800 2475
Wire Wire Line
	1725 2575 1800 2575
Text GLabel 1725 2675 0    50   BiDi ~ 0
H1_37
Text GLabel 1725 2775 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	1725 2675 1800 2675
Wire Wire Line
	1725 2775 1800 2775
Text GLabel 1725 2875 0    50   BiDi ~ 0
H1_41
Text GLabel 1725 2975 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	1725 2875 1800 2875
Wire Wire Line
	1725 2975 1800 2975
Text GLabel 1725 3075 0    50   BiDi ~ 0
H1_45
Text GLabel 1725 3175 0    50   BiDi ~ 10
LU_1
Wire Wire Line
	1725 3075 1800 3075
Wire Wire Line
	1725 3175 1800 3175
Text GLabel 1725 3275 0    50   BiDi ~ 10
LU_2
Text GLabel 1725 3375 0    50   BiDi ~ 10
LU_3
Wire Wire Line
	1725 3275 1800 3275
Wire Wire Line
	1725 3375 1800 3375
Wire Wire Line
	2300 875  2375 875 
Wire Wire Line
	2300 975  2375 975 
Text GLabel 2375 1075 2    50   BiDi ~ 0
H1_06
Text GLabel 2375 1175 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	2300 1075 2375 1075
Wire Wire Line
	2300 1175 2375 1175
Text GLabel 2375 1275 2    50   BiDi ~ 0
H1_10
Text GLabel 2375 1375 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	2300 1275 2375 1275
Wire Wire Line
	2300 1375 2375 1375
Text GLabel 2375 1475 2    50   BiDi ~ 0
H1_14
Text GLabel 2375 1575 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	2300 1475 2375 1475
Wire Wire Line
	2300 1575 2375 1575
Text GLabel 2375 1675 2    50   BiDi ~ 0
H1_18
Text GLabel 2375 1775 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	2300 1675 2375 1675
Wire Wire Line
	2300 1775 2375 1775
Text GLabel 2375 1875 2    50   BiDi ~ 0
H1_22
Text GLabel 2375 1975 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	2300 1875 2375 1875
Wire Wire Line
	2300 1975 2375 1975
Text GLabel 2375 2175 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	2300 2075 2375 2075
Wire Wire Line
	2300 2175 2375 2175
Text GLabel 2375 2275 2    50   BiDi ~ 0
H1_30
Text GLabel 2375 2375 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	2300 2275 2375 2275
Wire Wire Line
	2300 2375 2375 2375
Text GLabel 2375 2475 2    50   BiDi ~ 0
H1_34
Text GLabel 2375 2575 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	2300 2475 2375 2475
Wire Wire Line
	2300 2575 2375 2575
Text GLabel 2375 2675 2    50   BiDi ~ 0
H1_38
Text GLabel 2375 2775 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	2300 2675 2375 2675
Wire Wire Line
	2300 2775 2375 2775
Text GLabel 2375 2875 2    50   BiDi ~ 0
H1_42
Text GLabel 2375 2975 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	2300 2875 2375 2875
Wire Wire Line
	2300 2975 2375 2975
Text GLabel 2375 3075 2    50   BiDi ~ 0
H1_46
Text GLabel 2375 3175 2    50   BiDi ~ 10
LU_4
Wire Wire Line
	2300 3075 2375 3075
Wire Wire Line
	2300 3175 2375 3175
Text GLabel 2375 3275 2    50   BiDi ~ 10
LU_5
Text GLabel 2375 3375 2    50   BiDi ~ 10
LU_6
Wire Wire Line
	2300 3275 2375 3275
Wire Wire Line
	2300 3375 2375 3375
Text GLabel 3075 875  0    50   BiDi ~ 0
H2_01
Text GLabel 3075 975  0    50   BiDi ~ 0
H2_03
Wire Wire Line
	3075 875  3150 875 
Wire Wire Line
	3075 975  3150 975 
Text GLabel 3075 1075 0    50   BiDi ~ 0
H2_05
Text GLabel 3075 1175 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	3075 1075 3150 1075
Wire Wire Line
	3075 1175 3150 1175
Text GLabel 3075 1275 0    50   BiDi ~ 0
H2_09
Text GLabel 3075 1375 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	3075 1275 3150 1275
Wire Wire Line
	3075 1375 3150 1375
Text GLabel 3075 1475 0    50   BiDi ~ 0
H2_13
Text GLabel 3075 1575 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	3075 1475 3150 1475
Wire Wire Line
	3075 1575 3150 1575
Text GLabel 3075 1675 0    50   BiDi ~ 0
H2_17
Text GLabel 3075 1775 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	3075 1675 3150 1675
Wire Wire Line
	3075 1775 3150 1775
Text GLabel 3075 1875 0    50   BiDi ~ 0
H2_21
Text GLabel 3075 1975 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	3075 1875 3150 1875
Wire Wire Line
	3075 1975 3150 1975
Wire Wire Line
	3075 2075 3150 2075
Wire Wire Line
	3075 2175 3150 2175
Wire Wire Line
	3075 2275 3150 2275
Wire Wire Line
	3075 2375 3150 2375
Text GLabel 3075 2475 0    50   BiDi ~ 0
H2_33
Text GLabel 3075 2575 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	3075 2475 3150 2475
Wire Wire Line
	3075 2575 3150 2575
Text GLabel 3075 2675 0    50   BiDi ~ 0
H2_37
Text GLabel 3075 2775 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	3075 2675 3150 2675
Wire Wire Line
	3075 2775 3150 2775
Text GLabel 3075 2875 0    50   BiDi ~ 0
H2_41
Text GLabel 3075 2975 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	3075 2875 3150 2875
Wire Wire Line
	3075 2975 3150 2975
Text GLabel 3075 3175 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	3075 3075 3150 3075
Wire Wire Line
	3075 3175 3150 3175
Text GLabel 3075 3275 0    50   BiDi ~ 0
H2_49
Text GLabel 3075 3375 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	3075 3275 3150 3275
Wire Wire Line
	3075 3375 3150 3375
Text GLabel 3725 875  2    50   BiDi ~ 0
H2_02
Text GLabel 3725 975  2    50   BiDi ~ 0
H2_04
Wire Wire Line
	3650 875  3725 875 
Wire Wire Line
	3650 975  3725 975 
Text GLabel 3725 1075 2    50   BiDi ~ 0
H2_06
Text GLabel 3725 1175 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	3650 1075 3725 1075
Wire Wire Line
	3650 1175 3725 1175
Text GLabel 3725 1275 2    50   BiDi ~ 0
H2_10
Text GLabel 3725 1375 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	3650 1275 3725 1275
Wire Wire Line
	3650 1375 3725 1375
Text GLabel 3725 1475 2    50   BiDi ~ 0
H2_14
Text GLabel 3725 1575 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	3650 1475 3725 1475
Wire Wire Line
	3650 1575 3725 1575
Text GLabel 3725 1675 2    50   BiDi ~ 0
H2_18
Text GLabel 3725 1775 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	3650 1675 3725 1675
Wire Wire Line
	3650 1775 3725 1775
Text GLabel 3725 1875 2    50   BiDi ~ 0
H2_22
Text GLabel 3725 1975 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	3650 1875 3725 1875
Wire Wire Line
	3650 1975 3725 1975
Wire Wire Line
	3650 2075 3725 2075
Wire Wire Line
	3650 2175 3725 2175
Wire Wire Line
	3650 2275 3725 2275
Wire Wire Line
	3650 2375 3725 2375
Text GLabel 3725 2475 2    50   BiDi ~ 0
H2_34
Text GLabel 3725 2575 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	3650 2475 3725 2475
Wire Wire Line
	3650 2575 3725 2575
Text GLabel 3725 2675 2    50   BiDi ~ 0
H2_38
Text GLabel 3725 2775 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	3650 2675 3725 2675
Wire Wire Line
	3650 2775 3725 2775
Text GLabel 3725 2875 2    50   BiDi ~ 0
H2_42
Text GLabel 3725 2975 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	3650 2875 3725 2875
Wire Wire Line
	3650 2975 3725 2975
Text GLabel 3725 3175 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	3650 3075 3725 3075
Wire Wire Line
	3650 3175 3725 3175
Text GLabel 3725 3275 2    50   BiDi ~ 0
H2_50
Text GLabel 3725 3375 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	3650 3275 3725 3275
Wire Wire Line
	3650 3375 3725 3375
$Comp
L Mechanical:MountingHole_Pad M1_1
U 1 1 5F7CE2AE
P 875 975
F 0 "M1_1" H 750 1150 60  0000 L CNN
F 1 "HOLE" V 947 1030 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 875 975 60  0001 C CNN
F 3 "" H 875 975 60  0000 C CNN
	1    875  975 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M3_1
U 1 1 5F7D0057
P 875 1225
F 0 "M3_1" H 800 1425 60  0000 L CNN
F 1 "HOLE" V 947 1280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 875 1225 60  0001 C CNN
F 3 "" H 875 1225 60  0000 C CNN
	1    875  1225
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_2
U 1 1 5F9B9603
P 5425 2075
F 0 "H1_2" H 5450 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 5475 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 5425 3466 60  0001 C CNN
F 3 "" H 5425 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 5425 2075 50  0001 C CNN "MFPN"
	1    5425 2075
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_2
U 1 1 5F9B9609
P 6775 2075
F 0 "H2_2" H 6825 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 6825 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 6775 3466 60  0001 C CNN
F 3 "" H 6775 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 6775 2075 50  0001 C CNN "MFPN"
	1    6775 2075
	1    0    0    -1  
$EndComp
Text GLabel 5150 875  0    50   BiDi ~ 0
H1_01
Text GLabel 5800 875  2    50   BiDi ~ 0
H1_02
Text GLabel 5150 975  0    50   BiDi ~ 0
H1_03
Text GLabel 5800 975  2    50   BiDi ~ 0
H1_04
Wire Wire Line
	5150 875  5225 875 
Wire Wire Line
	5150 975  5225 975 
Text GLabel 5150 1075 0    50   BiDi ~ 0
H1_05
Text GLabel 5150 1175 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	5150 1075 5225 1075
Wire Wire Line
	5150 1175 5225 1175
Text GLabel 5150 1275 0    50   BiDi ~ 0
H1_09
Text GLabel 5150 1375 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	5150 1275 5225 1275
Wire Wire Line
	5150 1375 5225 1375
Text GLabel 5150 1475 0    50   BiDi ~ 0
H1_13
Text GLabel 5150 1575 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	5150 1475 5225 1475
Wire Wire Line
	5150 1575 5225 1575
Text GLabel 5150 1675 0    50   BiDi ~ 0
H1_17
Text GLabel 5150 1775 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	5150 1675 5225 1675
Wire Wire Line
	5150 1775 5225 1775
Text GLabel 5150 1875 0    50   BiDi ~ 0
H1_21
Text GLabel 5150 1975 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	5150 1875 5225 1875
Wire Wire Line
	5150 1975 5225 1975
Text GLabel 5150 2075 0    50   BiDi ~ 0
H1_25
Text GLabel 5150 2175 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	5150 2075 5225 2075
Wire Wire Line
	5150 2175 5225 2175
Text GLabel 5150 2275 0    50   BiDi ~ 0
H1_29
Text GLabel 5150 2375 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	5150 2275 5225 2275
Wire Wire Line
	5150 2375 5225 2375
Text GLabel 5150 2475 0    50   BiDi ~ 0
H1_33
Text GLabel 5150 2575 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	5150 2475 5225 2475
Wire Wire Line
	5150 2575 5225 2575
Text GLabel 5150 2675 0    50   BiDi ~ 0
H1_37
Text GLabel 5150 2775 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	5150 2675 5225 2675
Wire Wire Line
	5150 2775 5225 2775
Text GLabel 5150 2875 0    50   BiDi ~ 0
H1_41
Text GLabel 5150 2975 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	5150 2875 5225 2875
Wire Wire Line
	5150 2975 5225 2975
Text GLabel 5150 3075 0    50   BiDi ~ 0
H1_45
Wire Wire Line
	5150 3075 5225 3075
Wire Wire Line
	5150 3175 5225 3175
Wire Wire Line
	5150 3275 5225 3275
Wire Wire Line
	5150 3375 5225 3375
Wire Wire Line
	5725 875  5800 875 
Wire Wire Line
	5725 975  5800 975 
Text GLabel 5800 1075 2    50   BiDi ~ 0
H1_06
Text GLabel 5800 1175 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	5725 1075 5800 1075
Wire Wire Line
	5725 1175 5800 1175
Text GLabel 5800 1275 2    50   BiDi ~ 0
H1_10
Text GLabel 5800 1375 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	5725 1275 5800 1275
Wire Wire Line
	5725 1375 5800 1375
Text GLabel 5800 1475 2    50   BiDi ~ 0
H1_14
Text GLabel 5800 1575 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	5725 1475 5800 1475
Wire Wire Line
	5725 1575 5800 1575
Text GLabel 5800 1675 2    50   BiDi ~ 0
H1_18
Text GLabel 5800 1775 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	5725 1675 5800 1675
Wire Wire Line
	5725 1775 5800 1775
Text GLabel 5800 1875 2    50   BiDi ~ 0
H1_22
Text GLabel 5800 1975 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	5725 1875 5800 1875
Wire Wire Line
	5725 1975 5800 1975
Text GLabel 5800 2075 2    50   BiDi ~ 0
H1_26
Text GLabel 5800 2175 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	5725 2075 5800 2075
Wire Wire Line
	5725 2175 5800 2175
Text GLabel 5800 2275 2    50   BiDi ~ 0
H1_30
Text GLabel 5800 2375 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	5725 2275 5800 2275
Wire Wire Line
	5725 2375 5800 2375
Text GLabel 5800 2475 2    50   BiDi ~ 0
H1_34
Text GLabel 5800 2575 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	5725 2475 5800 2475
Wire Wire Line
	5725 2575 5800 2575
Text GLabel 5800 2675 2    50   BiDi ~ 0
H1_38
Text GLabel 5800 2775 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	5725 2675 5800 2675
Wire Wire Line
	5725 2775 5800 2775
Text GLabel 5800 2875 2    50   BiDi ~ 0
H1_42
Text GLabel 5800 2975 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	5725 2875 5800 2875
Wire Wire Line
	5725 2975 5800 2975
Text GLabel 5800 3075 2    50   BiDi ~ 0
H1_46
Wire Wire Line
	5725 3075 5800 3075
Wire Wire Line
	5725 3175 5800 3175
Wire Wire Line
	5725 3275 5800 3275
Wire Wire Line
	5725 3375 5800 3375
Text GLabel 6500 875  0    50   BiDi ~ 0
H2_01
Text GLabel 6500 975  0    50   BiDi ~ 0
H2_03
Wire Wire Line
	6500 875  6575 875 
Wire Wire Line
	6500 975  6575 975 
Text GLabel 6500 1075 0    50   BiDi ~ 0
H2_05
Text GLabel 6500 1175 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	6500 1075 6575 1075
Wire Wire Line
	6500 1175 6575 1175
Text GLabel 6500 1275 0    50   BiDi ~ 0
H2_09
Text GLabel 6500 1375 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	6500 1275 6575 1275
Wire Wire Line
	6500 1375 6575 1375
Text GLabel 6500 1475 0    50   BiDi ~ 0
H2_13
Text GLabel 6500 1575 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	6500 1475 6575 1475
Wire Wire Line
	6500 1575 6575 1575
Text GLabel 6500 1675 0    50   BiDi ~ 0
H2_17
Text GLabel 6500 1775 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	6500 1675 6575 1675
Wire Wire Line
	6500 1775 6575 1775
Text GLabel 6500 1875 0    50   BiDi ~ 0
H2_21
Text GLabel 6500 1975 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	6500 1875 6575 1875
Wire Wire Line
	6500 1975 6575 1975
Wire Wire Line
	6500 2075 6575 2075
Wire Wire Line
	6500 2175 6575 2175
Wire Wire Line
	6500 2275 6575 2275
Wire Wire Line
	6500 2375 6575 2375
Text GLabel 6500 2475 0    50   BiDi ~ 0
H2_33
Text GLabel 6500 2575 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	6500 2475 6575 2475
Wire Wire Line
	6500 2575 6575 2575
Text GLabel 6500 2675 0    50   BiDi ~ 0
H2_37
Text GLabel 6500 2775 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	6500 2675 6575 2675
Wire Wire Line
	6500 2775 6575 2775
Text GLabel 6500 2875 0    50   BiDi ~ 0
H2_41
Text GLabel 6500 2975 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	6500 2875 6575 2875
Wire Wire Line
	6500 2975 6575 2975
Text GLabel 6500 3075 0    50   BiDi ~ 10
V_BATT
Text GLabel 6500 3175 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	6500 3075 6575 3075
Wire Wire Line
	6500 3175 6575 3175
Text GLabel 6500 3275 0    50   BiDi ~ 0
H2_49
Text GLabel 6500 3375 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	6500 3275 6575 3275
Wire Wire Line
	6500 3375 6575 3375
Text GLabel 7150 875  2    50   BiDi ~ 0
H2_02
Text GLabel 7150 975  2    50   BiDi ~ 0
H2_04
Wire Wire Line
	7075 875  7150 875 
Wire Wire Line
	7075 975  7150 975 
Text GLabel 7150 1075 2    50   BiDi ~ 0
H2_06
Text GLabel 7150 1175 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	7075 1075 7150 1075
Wire Wire Line
	7075 1175 7150 1175
Text GLabel 7150 1275 2    50   BiDi ~ 0
H2_10
Text GLabel 7150 1375 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	7075 1275 7150 1275
Wire Wire Line
	7075 1375 7150 1375
Text GLabel 7150 1475 2    50   BiDi ~ 0
H2_14
Text GLabel 7150 1575 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	7075 1475 7150 1475
Wire Wire Line
	7075 1575 7150 1575
Text GLabel 7150 1675 2    50   BiDi ~ 0
H2_18
Text GLabel 7150 1775 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	7075 1675 7150 1675
Wire Wire Line
	7075 1775 7150 1775
Text GLabel 7150 1875 2    50   BiDi ~ 0
H2_22
Text GLabel 7150 1975 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	7075 1875 7150 1875
Wire Wire Line
	7075 1975 7150 1975
Wire Wire Line
	7075 2075 7150 2075
Wire Wire Line
	7075 2175 7150 2175
Wire Wire Line
	7075 2275 7150 2275
Wire Wire Line
	7075 2375 7150 2375
Text GLabel 7150 2475 2    50   BiDi ~ 0
H2_34
Text GLabel 7150 2575 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	7075 2475 7150 2475
Wire Wire Line
	7075 2575 7150 2575
Text GLabel 7150 2675 2    50   BiDi ~ 0
H2_38
Text GLabel 7150 2775 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	7075 2675 7150 2675
Wire Wire Line
	7075 2775 7150 2775
Text GLabel 7150 2875 2    50   BiDi ~ 0
H2_42
Text GLabel 7150 2975 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	7075 2875 7150 2875
Wire Wire Line
	7075 2975 7150 2975
Text GLabel 7150 3075 2    50   BiDi ~ 10
V_BATT
Text GLabel 7150 3175 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	7075 3075 7150 3075
Wire Wire Line
	7075 3175 7150 3175
Text GLabel 7150 3275 2    50   BiDi ~ 0
H2_50
Text GLabel 7150 3375 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	7075 3275 7150 3275
Wire Wire Line
	7075 3375 7150 3375
$Comp
L Mechanical:MountingHole_Pad M1_2
U 1 1 5F9B96DF
P 4300 975
F 0 "M1_2" H 4175 1150 60  0000 L CNN
F 1 "HOLE" V 4372 1030 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4300 975 60  0001 C CNN
F 3 "" H 4300 975 60  0000 C CNN
	1    4300 975 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M2_2
U 1 1 5F9B96E5
P 4550 975
F 0 "M2_2" H 4450 1150 60  0000 L CNN
F 1 "HOLE" V 4622 1030 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4550 975 60  0001 C CNN
F 3 "" H 4550 975 60  0000 C CNN
	1    4550 975 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M3_2
U 1 1 5F9B96EB
P 4300 1225
F 0 "M3_2" H 4225 1400 60  0000 L CNN
F 1 "HOLE" V 4372 1280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4300 1225 60  0001 C CNN
F 3 "" H 4300 1225 60  0000 C CNN
	1    4300 1225
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_3
U 1 1 5F9D4465
P 8850 2075
F 0 "H1_3" H 8850 3525 60  0000 C CNN
F 1 "HEADER_2x26" H 8900 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 8850 3466 60  0001 C CNN
F 3 "" H 8850 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 8850 2075 50  0001 C CNN "MFPN"
	1    8850 2075
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_3
U 1 1 5F9D446B
P 10200 2075
F 0 "H2_3" H 10225 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 10250 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 10200 3466 60  0001 C CNN
F 3 "" H 10200 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 10200 2075 50  0001 C CNN "MFPN"
	1    10200 2075
	1    0    0    -1  
$EndComp
Text GLabel 8575 875  0    50   BiDi ~ 0
H1_01
Text GLabel 9225 875  2    50   BiDi ~ 0
H1_02
Text GLabel 8575 975  0    50   BiDi ~ 0
H1_03
Text GLabel 9225 975  2    50   BiDi ~ 0
H1_04
Wire Wire Line
	8575 875  8650 875 
Wire Wire Line
	8575 975  8650 975 
Text GLabel 8575 1075 0    50   BiDi ~ 0
H1_05
Text GLabel 8575 1175 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	8575 1075 8650 1075
Wire Wire Line
	8575 1175 8650 1175
Text GLabel 8575 1275 0    50   BiDi ~ 0
H1_09
Text GLabel 8575 1375 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	8575 1275 8650 1275
Wire Wire Line
	8575 1375 8650 1375
Text GLabel 8575 1475 0    50   BiDi ~ 0
H1_13
Text GLabel 8575 1575 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	8575 1475 8650 1475
Wire Wire Line
	8575 1575 8650 1575
Text GLabel 8575 1675 0    50   BiDi ~ 0
H1_17
Text GLabel 8575 1775 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	8575 1675 8650 1675
Wire Wire Line
	8575 1775 8650 1775
Text GLabel 8575 1875 0    50   BiDi ~ 0
H1_21
Text GLabel 8575 1975 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	8575 1875 8650 1875
Wire Wire Line
	8575 1975 8650 1975
Text GLabel 8575 2075 0    50   BiDi ~ 0
H1_25
Text GLabel 8575 2175 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	8575 2075 8650 2075
Wire Wire Line
	8575 2175 8650 2175
Text GLabel 8575 2275 0    50   BiDi ~ 0
H1_29
Text GLabel 8575 2375 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	8575 2275 8650 2275
Wire Wire Line
	8575 2375 8650 2375
Text GLabel 8575 2475 0    50   BiDi ~ 0
H1_33
Text GLabel 8575 2575 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	8575 2475 8650 2475
Wire Wire Line
	8575 2575 8650 2575
Text GLabel 8575 2675 0    50   BiDi ~ 0
H1_37
Text GLabel 8575 2775 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	8575 2675 8650 2675
Wire Wire Line
	8575 2775 8650 2775
Text GLabel 8575 2875 0    50   BiDi ~ 0
H1_41
Text GLabel 8575 2975 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	8575 2875 8650 2875
Wire Wire Line
	8575 2975 8650 2975
Text GLabel 8575 3075 0    50   BiDi ~ 0
H1_45
Wire Wire Line
	8575 3075 8650 3075
Wire Wire Line
	8575 3175 8650 3175
Wire Wire Line
	8575 3275 8650 3275
Wire Wire Line
	8575 3375 8650 3375
Wire Wire Line
	9150 875  9225 875 
Wire Wire Line
	9150 975  9225 975 
Text GLabel 9225 1075 2    50   BiDi ~ 0
H1_06
Text GLabel 9225 1175 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	9150 1075 9225 1075
Wire Wire Line
	9150 1175 9225 1175
Text GLabel 9225 1275 2    50   BiDi ~ 0
H1_10
Text GLabel 9225 1375 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	9150 1275 9225 1275
Wire Wire Line
	9150 1375 9225 1375
Text GLabel 9225 1475 2    50   BiDi ~ 0
H1_14
Text GLabel 9225 1575 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	9150 1475 9225 1475
Wire Wire Line
	9150 1575 9225 1575
Text GLabel 9225 1675 2    50   BiDi ~ 0
H1_18
Text GLabel 9225 1775 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	9150 1675 9225 1675
Wire Wire Line
	9150 1775 9225 1775
Text GLabel 9225 1875 2    50   BiDi ~ 0
H1_22
Text GLabel 9225 1975 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	9150 1875 9225 1875
Wire Wire Line
	9150 1975 9225 1975
Text GLabel 9225 2075 2    50   BiDi ~ 0
H1_26
Text GLabel 9225 2175 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	9150 2075 9225 2075
Wire Wire Line
	9150 2175 9225 2175
Text GLabel 9225 2275 2    50   BiDi ~ 0
H1_30
Text GLabel 9225 2375 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	9150 2275 9225 2275
Wire Wire Line
	9150 2375 9225 2375
Text GLabel 9225 2475 2    50   BiDi ~ 0
H1_34
Text GLabel 9225 2575 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	9150 2475 9225 2475
Wire Wire Line
	9150 2575 9225 2575
Text GLabel 9225 2675 2    50   BiDi ~ 0
H1_38
Text GLabel 9225 2775 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	9150 2675 9225 2675
Wire Wire Line
	9150 2775 9225 2775
Text GLabel 9225 2875 2    50   BiDi ~ 0
H1_42
Text GLabel 9225 2975 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	9150 2875 9225 2875
Wire Wire Line
	9150 2975 9225 2975
Text GLabel 9225 3075 2    50   BiDi ~ 0
H1_46
Wire Wire Line
	9150 3075 9225 3075
Wire Wire Line
	9150 3175 9225 3175
Wire Wire Line
	9150 3275 9225 3275
Wire Wire Line
	9150 3375 9225 3375
Text GLabel 9925 875  0    50   BiDi ~ 0
H2_01
Text GLabel 9925 975  0    50   BiDi ~ 0
H2_03
Wire Wire Line
	9925 875  10000 875 
Wire Wire Line
	9925 975  10000 975 
Text GLabel 9925 1075 0    50   BiDi ~ 0
H2_05
Text GLabel 9925 1175 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	9925 1075 10000 1075
Wire Wire Line
	9925 1175 10000 1175
Text GLabel 9925 1275 0    50   BiDi ~ 0
H2_09
Text GLabel 9925 1375 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	9925 1275 10000 1275
Wire Wire Line
	9925 1375 10000 1375
Text GLabel 9925 1475 0    50   BiDi ~ 0
H2_13
Text GLabel 9925 1575 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	9925 1475 10000 1475
Wire Wire Line
	9925 1575 10000 1575
Text GLabel 9925 1675 0    50   BiDi ~ 0
H2_17
Text GLabel 9925 1775 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	9925 1675 10000 1675
Wire Wire Line
	9925 1775 10000 1775
Text GLabel 9925 1875 0    50   BiDi ~ 0
H2_21
Text GLabel 9925 1975 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	9925 1875 10000 1875
Wire Wire Line
	9925 1975 10000 1975
Wire Wire Line
	9925 2075 10000 2075
Wire Wire Line
	9925 2175 10000 2175
Wire Wire Line
	9925 2275 10000 2275
Wire Wire Line
	9925 2375 10000 2375
Text GLabel 9925 2475 0    50   BiDi ~ 0
H2_33
Text GLabel 9925 2575 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	9925 2475 10000 2475
Wire Wire Line
	9925 2575 10000 2575
Text GLabel 9925 2675 0    50   BiDi ~ 0
H2_37
Text GLabel 9925 2775 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	9925 2675 10000 2675
Wire Wire Line
	9925 2775 10000 2775
Text GLabel 9925 2875 0    50   BiDi ~ 0
H2_41
Text GLabel 9925 2975 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	9925 2875 10000 2875
Wire Wire Line
	9925 2975 10000 2975
Text GLabel 9925 3075 0    50   BiDi ~ 10
V_BATT
Text GLabel 9925 3175 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	9925 3075 10000 3075
Wire Wire Line
	9925 3175 10000 3175
Text GLabel 9925 3275 0    50   BiDi ~ 0
H2_49
Text GLabel 9925 3375 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	9925 3275 10000 3275
Wire Wire Line
	9925 3375 10000 3375
Text GLabel 10575 875  2    50   BiDi ~ 0
H2_02
Text GLabel 10575 975  2    50   BiDi ~ 0
H2_04
Wire Wire Line
	10500 875  10575 875 
Wire Wire Line
	10500 975  10575 975 
Text GLabel 10575 1075 2    50   BiDi ~ 0
H2_06
Text GLabel 10575 1175 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	10500 1075 10575 1075
Wire Wire Line
	10500 1175 10575 1175
Text GLabel 10575 1275 2    50   BiDi ~ 0
H2_10
Text GLabel 10575 1375 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	10500 1275 10575 1275
Wire Wire Line
	10500 1375 10575 1375
Text GLabel 10575 1475 2    50   BiDi ~ 0
H2_14
Text GLabel 10575 1575 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	10500 1475 10575 1475
Wire Wire Line
	10500 1575 10575 1575
Text GLabel 10575 1675 2    50   BiDi ~ 0
H2_18
Text GLabel 10575 1775 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	10500 1675 10575 1675
Wire Wire Line
	10500 1775 10575 1775
Text GLabel 10575 1875 2    50   BiDi ~ 0
H2_22
Text GLabel 10575 1975 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	10500 1875 10575 1875
Wire Wire Line
	10500 1975 10575 1975
Wire Wire Line
	10500 2075 10575 2075
Wire Wire Line
	10500 2175 10575 2175
Wire Wire Line
	10500 2275 10575 2275
Wire Wire Line
	10500 2375 10575 2375
Text GLabel 10575 2475 2    50   BiDi ~ 0
H2_34
Text GLabel 10575 2575 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	10500 2475 10575 2475
Wire Wire Line
	10500 2575 10575 2575
Text GLabel 10575 2675 2    50   BiDi ~ 0
H2_38
Text GLabel 10575 2775 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	10500 2675 10575 2675
Wire Wire Line
	10500 2775 10575 2775
Text GLabel 10575 2875 2    50   BiDi ~ 0
H2_42
Text GLabel 10575 2975 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	10500 2875 10575 2875
Wire Wire Line
	10500 2975 10575 2975
Text GLabel 10575 3075 2    50   BiDi ~ 10
V_BATT
Text GLabel 10575 3175 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	10500 3075 10575 3075
Wire Wire Line
	10500 3175 10575 3175
Text GLabel 10575 3275 2    50   BiDi ~ 0
H2_50
Text GLabel 10575 3375 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	10500 3275 10575 3275
Wire Wire Line
	10500 3375 10575 3375
Wire Notes Line
	700  550  10975 550 
Text GLabel 1725 3850 0    50   BiDi ~ 0
H1_01
Text GLabel 2375 3850 2    50   BiDi ~ 0
H1_02
Text GLabel 1725 3950 0    50   BiDi ~ 0
H1_03
Text GLabel 2375 3950 2    50   BiDi ~ 0
H1_04
Wire Wire Line
	1725 3850 1800 3850
Wire Wire Line
	1725 3950 1800 3950
Text GLabel 1725 4050 0    50   BiDi ~ 0
H1_05
Text GLabel 1725 4150 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	1725 4050 1800 4050
Wire Wire Line
	1725 4150 1800 4150
Text GLabel 1725 4250 0    50   BiDi ~ 0
H1_09
Text GLabel 1725 4350 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	1725 4250 1800 4250
Wire Wire Line
	1725 4350 1800 4350
Text GLabel 1725 4450 0    50   BiDi ~ 0
H1_13
Text GLabel 1725 4550 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	1725 4450 1800 4450
Wire Wire Line
	1725 4550 1800 4550
Text GLabel 1725 4650 0    50   BiDi ~ 0
H1_17
Text GLabel 1725 4750 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	1725 4650 1800 4650
Wire Wire Line
	1725 4750 1800 4750
Text GLabel 1725 4850 0    50   BiDi ~ 0
H1_21
Text GLabel 1725 4950 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	1725 4850 1800 4850
Wire Wire Line
	1725 4950 1800 4950
Text GLabel 1725 5050 0    50   BiDi ~ 0
H1_25
Text GLabel 1725 5150 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	1725 5050 1800 5050
Wire Wire Line
	1725 5150 1800 5150
Text GLabel 1725 5250 0    50   BiDi ~ 0
H1_29
Text GLabel 1725 5350 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	1725 5250 1800 5250
Wire Wire Line
	1725 5350 1800 5350
Text GLabel 1725 5450 0    50   BiDi ~ 0
H1_33
Text GLabel 1725 5550 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	1725 5450 1800 5450
Wire Wire Line
	1725 5550 1800 5550
Text GLabel 1725 5650 0    50   BiDi ~ 0
H1_37
Text GLabel 1725 5750 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	1725 5650 1800 5650
Wire Wire Line
	1725 5750 1800 5750
Text GLabel 1725 5850 0    50   BiDi ~ 0
H1_41
Text GLabel 1725 5950 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	1725 5850 1800 5850
Wire Wire Line
	1725 5950 1800 5950
Text GLabel 1725 6050 0    50   BiDi ~ 0
H1_45
Wire Wire Line
	1725 6050 1800 6050
Wire Wire Line
	1725 6150 1800 6150
Wire Wire Line
	1725 6250 1800 6250
Wire Wire Line
	1725 6350 1800 6350
Wire Wire Line
	2300 3850 2375 3850
Wire Wire Line
	2300 3950 2375 3950
Text GLabel 2375 4050 2    50   BiDi ~ 0
H1_06
Text GLabel 2375 4150 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	2300 4050 2375 4050
Wire Wire Line
	2300 4150 2375 4150
Text GLabel 2375 4250 2    50   BiDi ~ 0
H1_10
Text GLabel 2375 4350 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	2300 4250 2375 4250
Wire Wire Line
	2300 4350 2375 4350
Text GLabel 2375 4450 2    50   BiDi ~ 0
H1_14
Text GLabel 2375 4550 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	2300 4450 2375 4450
Wire Wire Line
	2300 4550 2375 4550
Text GLabel 2375 4650 2    50   BiDi ~ 0
H1_18
Text GLabel 2375 4750 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	2300 4650 2375 4650
Wire Wire Line
	2300 4750 2375 4750
Text GLabel 2375 4850 2    50   BiDi ~ 0
H1_22
Text GLabel 2375 4950 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	2300 4850 2375 4850
Wire Wire Line
	2300 4950 2375 4950
Text GLabel 2375 5050 2    50   BiDi ~ 0
H1_26
Text GLabel 2375 5150 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	2300 5050 2375 5050
Wire Wire Line
	2300 5150 2375 5150
Text GLabel 2375 5250 2    50   BiDi ~ 0
H1_30
Text GLabel 2375 5350 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	2300 5250 2375 5250
Wire Wire Line
	2300 5350 2375 5350
Text GLabel 2375 5450 2    50   BiDi ~ 0
H1_34
Text GLabel 2375 5550 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	2300 5450 2375 5450
Wire Wire Line
	2300 5550 2375 5550
Text GLabel 2375 5650 2    50   BiDi ~ 0
H1_38
Text GLabel 2375 5750 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	2300 5650 2375 5650
Wire Wire Line
	2300 5750 2375 5750
Text GLabel 2375 5850 2    50   BiDi ~ 0
H1_42
Text GLabel 2375 5950 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	2300 5850 2375 5850
Wire Wire Line
	2300 5950 2375 5950
Text GLabel 2375 6050 2    50   BiDi ~ 0
H1_46
Wire Wire Line
	2300 6050 2375 6050
Wire Wire Line
	2300 6150 2375 6150
Wire Wire Line
	2300 6250 2375 6250
Wire Wire Line
	2300 6350 2375 6350
Text GLabel 3075 3850 0    50   BiDi ~ 0
H2_01
Text GLabel 3075 3950 0    50   BiDi ~ 0
H2_03
Text GLabel 3075 4050 0    50   BiDi ~ 0
H2_05
Text GLabel 3075 4150 0    50   BiDi ~ 0
H2_07
Text GLabel 3075 4250 0    50   BiDi ~ 0
H2_09
Text GLabel 3075 4350 0    50   BiDi ~ 0
H2_11
Text GLabel 3075 4450 0    50   BiDi ~ 0
H2_13
Text GLabel 3075 4550 0    50   BiDi ~ 0
H2_15
Text GLabel 3075 4650 0    50   BiDi ~ 0
H2_17
Text GLabel 3075 4750 0    50   BiDi ~ 0
H2_19
Text GLabel 3075 4850 0    50   BiDi ~ 0
H2_21
Text GLabel 3075 4950 0    50   BiDi ~ 0
H2_23
Text GLabel 3075 5450 0    50   BiDi ~ 0
H2_33
Text GLabel 3075 5550 0    50   BiDi ~ 0
H2_35
Text GLabel 3075 5650 0    50   BiDi ~ 0
H2_37
Text GLabel 3075 5750 0    50   BiDi ~ 0
H2_39
Text GLabel 3075 5850 0    50   BiDi ~ 0
H2_41
Text GLabel 3075 5950 0    50   BiDi ~ 0
H2_43
Text GLabel 3075 6050 0    50   BiDi ~ 10
V_BATT
Text GLabel 3075 6150 0    50   BiDi ~ 0
H2_47
Text GLabel 3075 6250 0    50   BiDi ~ 0
H2_49
Text GLabel 3075 6350 0    50   BiDi ~ 0
H2_51
Text GLabel 3725 3850 2    50   BiDi ~ 0
H2_02
Text GLabel 3725 3950 2    50   BiDi ~ 0
H2_04
Wire Wire Line
	3650 3850 3725 3850
Wire Wire Line
	3650 3950 3725 3950
Text GLabel 3725 4050 2    50   BiDi ~ 0
H2_06
Text GLabel 3725 4150 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	3650 4050 3725 4050
Wire Wire Line
	3650 4150 3725 4150
Text GLabel 3725 4250 2    50   BiDi ~ 0
H2_10
Text GLabel 3725 4350 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	3650 4250 3725 4250
Wire Wire Line
	3650 4350 3725 4350
Text GLabel 3725 4450 2    50   BiDi ~ 0
H2_14
Text GLabel 3725 4550 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	3650 4450 3725 4450
Wire Wire Line
	3650 4550 3725 4550
Text GLabel 3725 4650 2    50   BiDi ~ 0
H2_18
Text GLabel 3725 4750 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	3650 4650 3725 4650
Wire Wire Line
	3650 4750 3725 4750
Text GLabel 3725 4850 2    50   BiDi ~ 0
H2_22
Text GLabel 3725 4950 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	3650 4850 3725 4850
Wire Wire Line
	3650 4950 3725 4950
Wire Wire Line
	3650 5050 3725 5050
Wire Wire Line
	3650 5150 3725 5150
Wire Wire Line
	3650 5250 3725 5250
Wire Wire Line
	3650 5350 3725 5350
Text GLabel 3725 5450 2    50   BiDi ~ 0
H2_34
Text GLabel 3725 5550 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	3650 5450 3725 5450
Wire Wire Line
	3650 5550 3725 5550
Text GLabel 3725 5650 2    50   BiDi ~ 0
H2_38
Text GLabel 3725 5750 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	3650 5650 3725 5650
Wire Wire Line
	3650 5750 3725 5750
Text GLabel 3725 5850 2    50   BiDi ~ 0
H2_42
Text GLabel 3725 5950 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	3650 5850 3725 5850
Wire Wire Line
	3650 5950 3725 5950
Text GLabel 3725 6050 2    50   BiDi ~ 10
V_BATT
Text GLabel 3725 6150 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	3650 6050 3725 6050
Wire Wire Line
	3650 6150 3725 6150
Text GLabel 3725 6250 2    50   BiDi ~ 0
H2_50
Text GLabel 3725 6350 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	3650 6250 3725 6250
Wire Wire Line
	3650 6350 3725 6350
$Comp
L Mechanical:MountingHole_Pad M1_4
U 1 1 5FA0325B
P 875 3950
F 0 "M1_4" H 725 4125 60  0000 L CNN
F 1 "HOLE" V 947 4005 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 875 3950 60  0001 C CNN
F 3 "" H 875 3950 60  0000 C CNN
	1    875  3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M2_4
U 1 1 5FA03261
P 1125 3950
F 0 "M2_4" H 1025 4125 60  0000 L CNN
F 1 "HOLE" V 1197 4005 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1125 3950 60  0001 C CNN
F 3 "" H 1125 3950 60  0000 C CNN
	1    1125 3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M3_4
U 1 1 5FA03267
P 875 4200
F 0 "M3_4" H 800 4375 60  0000 L CNN
F 1 "HOLE" V 947 4255 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 875 4200 60  0001 C CNN
F 3 "" H 875 4200 60  0000 C CNN
	1    875  4200
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M4_4
U 1 1 5FA0326D
P 1125 4200
F 0 "M4_4" H 950 4375 60  0000 L CNN
F 1 "HOLE" V 1197 4255 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1125 4200 60  0001 C CNN
F 3 "" H 1125 4200 60  0000 C CNN
	1    1125 4200
	-1   0    0    1   
$EndComp
Wire Notes Line
	700  550  700  6500
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_5
U 1 1 5FA5C2A9
P 5425 5050
F 0 "H1_5" H 5475 6475 60  0000 C CNN
F 1 "HEADER_2x26" H 5475 6375 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 5425 6441 60  0001 C CNN
F 3 "" H 5425 6300 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 5425 5050 50  0001 C CNN "MFPN"
	1    5425 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_5
U 1 1 5FA5C2AF
P 6775 5050
F 0 "H2_5" H 6825 6475 60  0000 C CNN
F 1 "HEADER_2x26" H 6825 6375 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 6775 6441 60  0001 C CNN
F 3 "" H 6775 6300 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 6775 5050 50  0001 C CNN "MFPN"
	1    6775 5050
	1    0    0    -1  
$EndComp
Text GLabel 5150 3850 0    50   BiDi ~ 0
H1_01
Text GLabel 5800 3850 2    50   BiDi ~ 0
H1_02
Text GLabel 5150 3950 0    50   BiDi ~ 0
H1_03
Text GLabel 5800 3950 2    50   BiDi ~ 0
H1_04
Wire Wire Line
	5150 3850 5225 3850
Wire Wire Line
	5150 3950 5225 3950
Text GLabel 5150 4050 0    50   BiDi ~ 0
H1_05
Text GLabel 5150 4150 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	5150 4050 5225 4050
Wire Wire Line
	5150 4150 5225 4150
Text GLabel 5150 4250 0    50   BiDi ~ 0
H1_09
Text GLabel 5150 4350 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	5150 4250 5225 4250
Wire Wire Line
	5150 4350 5225 4350
Text GLabel 5150 4450 0    50   BiDi ~ 0
H1_13
Text GLabel 5150 4550 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	5150 4450 5225 4450
Wire Wire Line
	5150 4550 5225 4550
Text GLabel 5150 4650 0    50   BiDi ~ 0
H1_17
Text GLabel 5150 4750 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	5150 4650 5225 4650
Wire Wire Line
	5150 4750 5225 4750
Text GLabel 5150 4850 0    50   BiDi ~ 0
H1_21
Text GLabel 5150 4950 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	5150 4850 5225 4850
Wire Wire Line
	5150 4950 5225 4950
Text GLabel 5150 5050 0    50   BiDi ~ 0
H1_25
Text GLabel 5150 5150 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	5150 5050 5225 5050
Wire Wire Line
	5150 5150 5225 5150
Text GLabel 5150 5250 0    50   BiDi ~ 0
H1_29
Text GLabel 5150 5350 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	5150 5250 5225 5250
Wire Wire Line
	5150 5350 5225 5350
Text GLabel 5150 5450 0    50   BiDi ~ 0
H1_33
Text GLabel 5150 5550 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	5150 5450 5225 5450
Wire Wire Line
	5150 5550 5225 5550
Text GLabel 5150 5650 0    50   BiDi ~ 0
H1_37
Text GLabel 5150 5750 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	5150 5650 5225 5650
Wire Wire Line
	5150 5750 5225 5750
Text GLabel 5150 5850 0    50   BiDi ~ 0
H1_41
Text GLabel 5150 5950 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	5150 5850 5225 5850
Wire Wire Line
	5150 5950 5225 5950
Text GLabel 5150 6050 0    50   BiDi ~ 0
H1_45
Wire Wire Line
	5150 6050 5225 6050
Wire Wire Line
	5150 6150 5225 6150
Wire Wire Line
	5150 6250 5225 6250
Wire Wire Line
	5150 6350 5225 6350
Wire Wire Line
	5725 3850 5800 3850
Wire Wire Line
	5725 3950 5800 3950
Text GLabel 5800 4050 2    50   BiDi ~ 0
H1_06
Text GLabel 5800 4150 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	5725 4050 5800 4050
Wire Wire Line
	5725 4150 5800 4150
Text GLabel 5800 4250 2    50   BiDi ~ 0
H1_10
Text GLabel 5800 4350 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	5725 4250 5800 4250
Wire Wire Line
	5725 4350 5800 4350
Text GLabel 5800 4450 2    50   BiDi ~ 0
H1_14
Text GLabel 5800 4550 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	5725 4450 5800 4450
Wire Wire Line
	5725 4550 5800 4550
Text GLabel 5800 4650 2    50   BiDi ~ 0
H1_18
Text GLabel 5800 4750 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	5725 4650 5800 4650
Wire Wire Line
	5725 4750 5800 4750
Text GLabel 5800 4850 2    50   BiDi ~ 0
H1_22
Text GLabel 5800 4950 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	5725 4850 5800 4850
Wire Wire Line
	5725 4950 5800 4950
Text GLabel 5800 5050 2    50   BiDi ~ 0
H1_26
Text GLabel 5800 5150 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	5725 5050 5800 5050
Wire Wire Line
	5725 5150 5800 5150
Text GLabel 5800 5250 2    50   BiDi ~ 0
H1_30
Text GLabel 5800 5350 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	5725 5250 5800 5250
Wire Wire Line
	5725 5350 5800 5350
Text GLabel 5800 5450 2    50   BiDi ~ 0
H1_34
Text GLabel 5800 5550 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	5725 5450 5800 5450
Wire Wire Line
	5725 5550 5800 5550
Text GLabel 5800 5650 2    50   BiDi ~ 0
H1_38
Text GLabel 5800 5750 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	5725 5650 5800 5650
Wire Wire Line
	5725 5750 5800 5750
Text GLabel 5800 5850 2    50   BiDi ~ 0
H1_42
Text GLabel 5800 5950 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	5725 5850 5800 5850
Wire Wire Line
	5725 5950 5800 5950
Text GLabel 5800 6050 2    50   BiDi ~ 0
H1_46
Wire Wire Line
	5725 6050 5800 6050
Wire Wire Line
	5725 6150 5800 6150
Wire Wire Line
	5725 6250 5800 6250
Wire Wire Line
	5725 6350 5800 6350
Text GLabel 6500 3850 0    50   BiDi ~ 0
H2_01
Text GLabel 6500 3950 0    50   BiDi ~ 0
H2_03
Wire Wire Line
	6500 3850 6575 3850
Wire Wire Line
	6500 3950 6575 3950
Text GLabel 6500 4050 0    50   BiDi ~ 0
H2_05
Text GLabel 6500 4150 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	6500 4050 6575 4050
Wire Wire Line
	6500 4150 6575 4150
Text GLabel 6500 4250 0    50   BiDi ~ 0
H2_09
Text GLabel 6500 4350 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	6500 4250 6575 4250
Wire Wire Line
	6500 4350 6575 4350
Text GLabel 6500 4450 0    50   BiDi ~ 0
H2_13
Text GLabel 6500 4550 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	6500 4450 6575 4450
Wire Wire Line
	6500 4550 6575 4550
Text GLabel 6500 4650 0    50   BiDi ~ 0
H2_17
Text GLabel 6500 4750 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	6500 4650 6575 4650
Wire Wire Line
	6500 4750 6575 4750
Text GLabel 6500 4850 0    50   BiDi ~ 0
H2_21
Text GLabel 6500 4950 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	6500 4850 6575 4850
Wire Wire Line
	6500 4950 6575 4950
Wire Wire Line
	6500 5050 6575 5050
Wire Wire Line
	6500 5150 6575 5150
Wire Wire Line
	6500 5250 6575 5250
Wire Wire Line
	6500 5350 6575 5350
Text GLabel 6500 5450 0    50   BiDi ~ 0
H2_33
Text GLabel 6500 5550 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	6500 5450 6575 5450
Wire Wire Line
	6500 5550 6575 5550
Text GLabel 6500 5650 0    50   BiDi ~ 0
H2_37
Text GLabel 6500 5750 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	6500 5650 6575 5650
Wire Wire Line
	6500 5750 6575 5750
Text GLabel 6500 5850 0    50   BiDi ~ 0
H2_41
Text GLabel 6500 5950 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	6500 5850 6575 5850
Wire Wire Line
	6500 5950 6575 5950
Text GLabel 6500 6050 0    50   BiDi ~ 10
V_BATT
Text GLabel 6500 6150 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	6500 6050 6575 6050
Wire Wire Line
	6500 6150 6575 6150
Text GLabel 6500 6250 0    50   BiDi ~ 0
H2_49
Text GLabel 6500 6350 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	6500 6250 6575 6250
Wire Wire Line
	6500 6350 6575 6350
Text GLabel 7150 3850 2    50   BiDi ~ 0
H2_02
Text GLabel 7150 3950 2    50   BiDi ~ 0
H2_04
Wire Wire Line
	7075 3850 7150 3850
Wire Wire Line
	7075 3950 7150 3950
Text GLabel 7150 4050 2    50   BiDi ~ 0
H2_06
Text GLabel 7150 4150 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	7075 4050 7150 4050
Wire Wire Line
	7075 4150 7150 4150
Text GLabel 7150 4250 2    50   BiDi ~ 0
H2_10
Text GLabel 7150 4350 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	7075 4250 7150 4250
Wire Wire Line
	7075 4350 7150 4350
Text GLabel 7150 4450 2    50   BiDi ~ 0
H2_14
Text GLabel 7150 4550 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	7075 4450 7150 4450
Wire Wire Line
	7075 4550 7150 4550
Text GLabel 7150 4650 2    50   BiDi ~ 0
H2_18
Text GLabel 7150 4750 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	7075 4650 7150 4650
Wire Wire Line
	7075 4750 7150 4750
Text GLabel 7150 4850 2    50   BiDi ~ 0
H2_22
Text GLabel 7150 4950 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	7075 4850 7150 4850
Wire Wire Line
	7075 4950 7150 4950
Wire Wire Line
	7075 5050 7150 5050
Wire Wire Line
	7075 5150 7150 5150
Wire Wire Line
	7075 5250 7150 5250
Wire Wire Line
	7075 5350 7150 5350
Text GLabel 7150 5450 2    50   BiDi ~ 0
H2_34
Text GLabel 7150 5550 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	7075 5450 7150 5450
Wire Wire Line
	7075 5550 7150 5550
Text GLabel 7150 5650 2    50   BiDi ~ 0
H2_38
Text GLabel 7150 5750 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	7075 5650 7150 5650
Wire Wire Line
	7075 5750 7150 5750
Text GLabel 7150 5850 2    50   BiDi ~ 0
H2_42
Text GLabel 7150 5950 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	7075 5850 7150 5850
Wire Wire Line
	7075 5950 7150 5950
Text GLabel 7150 6050 2    50   BiDi ~ 10
V_BATT
Text GLabel 7150 6150 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	7075 6050 7150 6050
Wire Wire Line
	7075 6150 7150 6150
Text GLabel 7150 6250 2    50   BiDi ~ 0
H2_50
Text GLabel 7150 6350 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	7075 6250 7150 6250
Wire Wire Line
	7075 6350 7150 6350
$Comp
L Mechanical:MountingHole_Pad M1_5
U 1 1 5FA5C385
P 4300 3950
F 0 "M1_5" H 4150 4125 60  0000 L CNN
F 1 "HOLE" V 4372 4005 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4300 3950 60  0001 C CNN
F 3 "" H 4300 3950 60  0000 C CNN
	1    4300 3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M2_5
U 1 1 5FA5C38B
P 4550 3950
F 0 "M2_5" H 4450 4125 60  0000 L CNN
F 1 "HOLE" V 4622 4005 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4550 3950 60  0001 C CNN
F 3 "" H 4550 3950 60  0000 C CNN
	1    4550 3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M3_5
U 1 1 5FA5C391
P 4300 4200
F 0 "M3_5" H 4225 4375 60  0000 L CNN
F 1 "HOLE" V 4372 4255 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4300 4200 60  0001 C CNN
F 3 "" H 4300 4200 60  0000 C CNN
	1    4300 4200
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M4_5
U 1 1 5FA5C397
P 4550 4200
F 0 "M4_5" H 4375 4375 60  0000 L CNN
F 1 "HOLE" V 4622 4255 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4550 4200 60  0001 C CNN
F 3 "" H 4550 4200 60  0000 C CNN
	1    4550 4200
	-1   0    0    1   
$EndComp
Wire Notes Line
	4125 550  4125 6500
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_6
U 1 1 5FAE6E67
P 8850 5050
F 0 "H1_6" H 8900 6475 60  0000 C CNN
F 1 "HEADER_2x26" H 8900 6375 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 8850 6441 60  0001 C CNN
F 3 "" H 8850 6300 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 8850 5050 50  0001 C CNN "MFPN"
	1    8850 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_6
U 1 1 5FAE6E6D
P 10200 5050
F 0 "H2_6" H 10225 6475 60  0000 C CNN
F 1 "HEADER_2x26" H 10250 6375 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 10200 6441 60  0001 C CNN
F 3 "" H 10200 6300 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 10200 5050 50  0001 C CNN "MFPN"
	1    10200 5050
	1    0    0    -1  
$EndComp
Text GLabel 8575 3850 0    50   BiDi ~ 0
H1_01
Text GLabel 9225 3850 2    50   BiDi ~ 0
H1_02
Text GLabel 8575 3950 0    50   BiDi ~ 0
H1_03
Text GLabel 9225 3950 2    50   BiDi ~ 0
H1_04
Wire Wire Line
	8575 3850 8650 3850
Wire Wire Line
	8575 3950 8650 3950
Text GLabel 8575 4050 0    50   BiDi ~ 0
H1_05
Text GLabel 8575 4150 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	8575 4050 8650 4050
Wire Wire Line
	8575 4150 8650 4150
Text GLabel 8575 4250 0    50   BiDi ~ 0
H1_09
Text GLabel 8575 4350 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	8575 4250 8650 4250
Wire Wire Line
	8575 4350 8650 4350
Text GLabel 8575 4450 0    50   BiDi ~ 0
H1_13
Text GLabel 8575 4550 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	8575 4450 8650 4450
Wire Wire Line
	8575 4550 8650 4550
Text GLabel 8575 4650 0    50   BiDi ~ 0
H1_17
Text GLabel 8575 4750 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	8575 4650 8650 4650
Wire Wire Line
	8575 4750 8650 4750
Text GLabel 8575 4850 0    50   BiDi ~ 0
H1_21
Text GLabel 8575 4950 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	8575 4850 8650 4850
Wire Wire Line
	8575 4950 8650 4950
Text GLabel 8575 5050 0    50   BiDi ~ 0
H1_25
Text GLabel 8575 5150 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	8575 5050 8650 5050
Wire Wire Line
	8575 5150 8650 5150
Text GLabel 8575 5250 0    50   BiDi ~ 0
H1_29
Text GLabel 8575 5350 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	8575 5250 8650 5250
Wire Wire Line
	8575 5350 8650 5350
Text GLabel 8575 5450 0    50   BiDi ~ 0
H1_33
Text GLabel 8575 5550 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	8575 5450 8650 5450
Wire Wire Line
	8575 5550 8650 5550
Text GLabel 8575 5650 0    50   BiDi ~ 0
H1_37
Text GLabel 8575 5750 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	8575 5650 8650 5650
Wire Wire Line
	8575 5750 8650 5750
Text GLabel 8575 5850 0    50   BiDi ~ 0
H1_41
Text GLabel 8575 5950 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	8575 5850 8650 5850
Wire Wire Line
	8575 5950 8650 5950
Text GLabel 8575 6050 0    50   BiDi ~ 0
H1_45
Wire Wire Line
	8575 6050 8650 6050
Wire Wire Line
	8575 6150 8650 6150
Wire Wire Line
	8575 6250 8650 6250
Wire Wire Line
	8575 6350 8650 6350
Wire Wire Line
	9150 3850 9225 3850
Wire Wire Line
	9150 3950 9225 3950
Text GLabel 9225 4050 2    50   BiDi ~ 0
H1_06
Text GLabel 9225 4150 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	9150 4050 9225 4050
Wire Wire Line
	9150 4150 9225 4150
Text GLabel 9225 4250 2    50   BiDi ~ 0
H1_10
Text GLabel 9225 4350 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	9150 4250 9225 4250
Wire Wire Line
	9150 4350 9225 4350
Text GLabel 9225 4450 2    50   BiDi ~ 0
H1_14
Text GLabel 9225 4550 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	9150 4450 9225 4450
Wire Wire Line
	9150 4550 9225 4550
Text GLabel 9225 4650 2    50   BiDi ~ 0
H1_18
Text GLabel 9225 4750 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	9150 4650 9225 4650
Wire Wire Line
	9150 4750 9225 4750
Text GLabel 9225 4850 2    50   BiDi ~ 0
H1_22
Text GLabel 9225 4950 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	9150 4850 9225 4850
Wire Wire Line
	9150 4950 9225 4950
Text GLabel 9225 5050 2    50   BiDi ~ 0
H1_26
Text GLabel 9225 5150 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	9150 5050 9225 5050
Wire Wire Line
	9150 5150 9225 5150
Text GLabel 9225 5250 2    50   BiDi ~ 0
H1_30
Text GLabel 9225 5350 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	9150 5250 9225 5250
Wire Wire Line
	9150 5350 9225 5350
Text GLabel 9225 5450 2    50   BiDi ~ 0
H1_34
Text GLabel 9225 5550 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	9150 5450 9225 5450
Wire Wire Line
	9150 5550 9225 5550
Text GLabel 9225 5650 2    50   BiDi ~ 0
H1_38
Text GLabel 9225 5750 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	9150 5650 9225 5650
Wire Wire Line
	9150 5750 9225 5750
Text GLabel 9225 5850 2    50   BiDi ~ 0
H1_42
Text GLabel 9225 5950 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	9150 5850 9225 5850
Wire Wire Line
	9150 5950 9225 5950
Text GLabel 9225 6050 2    50   BiDi ~ 0
H1_46
Wire Wire Line
	9150 6050 9225 6050
Wire Wire Line
	9150 6150 9225 6150
Wire Wire Line
	9150 6250 9225 6250
Wire Wire Line
	9150 6350 9225 6350
Text GLabel 9925 3850 0    50   BiDi ~ 0
H2_01
Text GLabel 9925 3950 0    50   BiDi ~ 0
H2_03
Wire Wire Line
	9925 3850 10000 3850
Wire Wire Line
	9925 3950 10000 3950
Text GLabel 9925 4050 0    50   BiDi ~ 0
H2_05
Text GLabel 9925 4150 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	9925 4050 10000 4050
Wire Wire Line
	9925 4150 10000 4150
Text GLabel 9925 4250 0    50   BiDi ~ 0
H2_09
Text GLabel 9925 4350 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	9925 4250 10000 4250
Wire Wire Line
	9925 4350 10000 4350
Text GLabel 9925 4450 0    50   BiDi ~ 0
H2_13
Text GLabel 9925 4550 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	9925 4450 10000 4450
Wire Wire Line
	9925 4550 10000 4550
Text GLabel 9925 4650 0    50   BiDi ~ 0
H2_17
Text GLabel 9925 4750 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	9925 4650 10000 4650
Wire Wire Line
	9925 4750 10000 4750
Text GLabel 9925 4850 0    50   BiDi ~ 0
H2_21
Text GLabel 9925 4950 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	9925 4850 10000 4850
Wire Wire Line
	9925 4950 10000 4950
Wire Wire Line
	9925 5050 10000 5050
Wire Wire Line
	9925 5150 10000 5150
Wire Wire Line
	9925 5250 10000 5250
Wire Wire Line
	9925 5350 10000 5350
Text GLabel 9925 5450 0    50   BiDi ~ 0
H2_33
Text GLabel 9925 5550 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	9925 5450 10000 5450
Wire Wire Line
	9925 5550 10000 5550
Text GLabel 9925 5650 0    50   BiDi ~ 0
H2_37
Text GLabel 9925 5750 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	9925 5650 10000 5650
Wire Wire Line
	9925 5750 10000 5750
Text GLabel 9925 5850 0    50   BiDi ~ 0
H2_41
Text GLabel 9925 5950 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	9925 5850 10000 5850
Wire Wire Line
	9925 5950 10000 5950
Text GLabel 9925 6050 0    50   BiDi ~ 10
V_BATT
Text GLabel 9925 6150 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	9925 6050 10000 6050
Wire Wire Line
	9925 6150 10000 6150
Text GLabel 9925 6250 0    50   BiDi ~ 0
H2_49
Text GLabel 9925 6350 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	9925 6250 10000 6250
Wire Wire Line
	9925 6350 10000 6350
Text GLabel 10575 3850 2    50   BiDi ~ 0
H2_02
Text GLabel 10575 3950 2    50   BiDi ~ 0
H2_04
Wire Wire Line
	10500 3850 10575 3850
Wire Wire Line
	10500 3950 10575 3950
Text GLabel 10575 4050 2    50   BiDi ~ 0
H2_06
Text GLabel 10575 4150 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	10500 4050 10575 4050
Wire Wire Line
	10500 4150 10575 4150
Text GLabel 10575 4250 2    50   BiDi ~ 0
H2_10
Text GLabel 10575 4350 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	10500 4250 10575 4250
Wire Wire Line
	10500 4350 10575 4350
Text GLabel 10575 4450 2    50   BiDi ~ 0
H2_14
Text GLabel 10575 4550 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	10500 4450 10575 4450
Wire Wire Line
	10500 4550 10575 4550
Text GLabel 10575 4650 2    50   BiDi ~ 0
H2_18
Text GLabel 10575 4750 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	10500 4650 10575 4650
Wire Wire Line
	10500 4750 10575 4750
Text GLabel 10575 4850 2    50   BiDi ~ 0
H2_22
Text GLabel 10575 4950 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	10500 4850 10575 4850
Wire Wire Line
	10500 4950 10575 4950
Wire Wire Line
	10500 5050 10575 5050
Wire Wire Line
	10500 5150 10575 5150
Wire Wire Line
	10500 5250 10575 5250
Wire Wire Line
	10500 5350 10575 5350
Text GLabel 10575 5450 2    50   BiDi ~ 0
H2_34
Text GLabel 10575 5550 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	10500 5450 10575 5450
Wire Wire Line
	10500 5550 10575 5550
Text GLabel 10575 5650 2    50   BiDi ~ 0
H2_38
Text GLabel 10575 5750 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	10500 5650 10575 5650
Wire Wire Line
	10500 5750 10575 5750
Text GLabel 10575 5850 2    50   BiDi ~ 0
H2_42
Text GLabel 10575 5950 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	10500 5850 10575 5850
Wire Wire Line
	10500 5950 10575 5950
Text GLabel 10575 6050 2    50   BiDi ~ 10
V_BATT
Text GLabel 10575 6150 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	10500 6050 10575 6050
Wire Wire Line
	10500 6150 10575 6150
Text GLabel 10575 6250 2    50   BiDi ~ 0
H2_50
Text GLabel 10575 6350 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	10500 6250 10575 6250
Wire Wire Line
	10500 6350 10575 6350
$Comp
L Mechanical:MountingHole_Pad M1_6
U 1 1 5FAE6F43
P 7725 3950
F 0 "M1_6" H 7575 4125 60  0000 L CNN
F 1 "HOLE" V 7797 4005 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7725 3950 60  0001 C CNN
F 3 "" H 7725 3950 60  0000 C CNN
	1    7725 3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M2_6
U 1 1 5FAE6F49
P 7975 3950
F 0 "M2_6" H 7875 4125 60  0000 L CNN
F 1 "HOLE" V 8047 4005 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7975 3950 60  0001 C CNN
F 3 "" H 7975 3950 60  0000 C CNN
	1    7975 3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M3_6
U 1 1 5FAE6F4F
P 7725 4200
F 0 "M3_6" H 7675 4375 60  0000 L CNN
F 1 "HOLE" V 7797 4255 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7725 4200 60  0001 C CNN
F 3 "" H 7725 4200 60  0000 C CNN
	1    7725 4200
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M4_6
U 1 1 5FAE6F55
P 7975 4200
F 0 "M4_6" H 7800 4375 60  0000 L CNN
F 1 "HOLE" V 8047 4255 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7975 4200 60  0001 C CNN
F 3 "" H 7975 4200 60  0000 C CNN
	1    7975 4200
	-1   0    0    1   
$EndComp
Wire Notes Line
	10975 550  10975 6500
Wire Notes Line
	7550 550  7550 6500
Wire Notes Line
	700  6500 10975 6500
Wire Notes Line
	700  3525 10975 3525
$Comp
L Mechanical:MountingHole_Pad M1
U 1 1 60DF08E4
P 975 6825
F 0 "M1" H 900 7000 60  0000 L CNN
F 1 "HOLE" V 1047 6880 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 975 6825 60  0001 C CNN
F 3 "" H 975 6825 60  0000 C CNN
	1    975  6825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M2
U 1 1 60DF08EA
P 1225 6825
F 0 "M2" H 1150 7000 60  0000 L CNN
F 1 "HOLE" V 1297 6880 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1225 6825 60  0001 C CNN
F 3 "" H 1225 6825 60  0000 C CNN
	1    1225 6825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M3
U 1 1 60DF08F0
P 975 7225
F 0 "M3" H 925 7400 60  0000 L CNN
F 1 "HOLE" V 1047 7280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 975 7225 60  0001 C CNN
F 3 "" H 975 7225 60  0000 C CNN
	1    975  7225
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M4
U 1 1 60DF08F6
P 1225 7225
F 0 "M4" H 1150 7400 60  0000 L CNN
F 1 "HOLE" V 1297 7280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1225 7225 60  0001 C CNN
F 3 "" H 1225 7225 60  0000 C CNN
	1    1225 7225
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M5
U 1 1 60EA0BBB
P 1525 6825
F 0 "M5" H 1450 7000 60  0000 L CNN
F 1 "HOLE" V 1597 6880 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1525 6825 60  0001 C CNN
F 3 "" H 1525 6825 60  0000 C CNN
	1    1525 6825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M6
U 1 1 60EA0BC7
P 1775 6825
F 0 "M6" H 1700 7000 60  0000 L CNN
F 1 "HOLE" V 1847 6880 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1775 6825 60  0001 C CNN
F 3 "" H 1775 6825 60  0000 C CNN
	1    1775 6825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M7
U 1 1 613338E4
P 1475 7225
F 0 "M7" H 1425 7400 60  0000 L CNN
F 1 "HOLE" V 1547 7280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1475 7225 60  0001 C CNN
F 3 "" H 1475 7225 60  0000 C CNN
	1    1475 7225
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M8
U 1 1 613338EA
P 1850 7225
F 0 "M8" H 1775 7400 60  0000 L CNN
F 1 "HOLE" V 1922 7280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1850 7225 60  0001 C CNN
F 3 "" H 1850 7225 60  0000 C CNN
	1    1850 7225
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M9
U 1 1 613EDADB
P 2075 6825
F 0 "M9" H 2000 7000 60  0000 L CNN
F 1 "HOLE" V 2147 6880 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 2075 6825 60  0001 C CNN
F 3 "" H 2075 6825 60  0000 C CNN
	1    2075 6825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M10
U 1 1 613EDAE1
P 2325 6825
F 0 "M10" H 2225 7000 60  0000 L CNN
F 1 "HOLE" V 2397 6880 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 2325 6825 60  0001 C CNN
F 3 "" H 2325 6825 60  0000 C CNN
	1    2325 6825
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M11
U 1 1 613EDAE9
P 2075 7225
F 0 "M11" H 1975 7400 60  0000 L CNN
F 1 "HOLE" V 2147 7280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 2075 7225 60  0001 C CNN
F 3 "" H 2075 7225 60  0000 C CNN
	1    2075 7225
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M12
U 1 1 613EDAEF
P 2325 7225
F 0 "M12" H 2250 7400 60  0000 L CNN
F 1 "HOLE" V 2397 7280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 2325 7225 60  0001 C CNN
F 3 "" H 2325 7225 60  0000 C CNN
	1    2325 7225
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_7
U 1 1 6165C256
P 12500 2075
F 0 "H1_7" H 12550 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 12550 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 12500 3466 60  0001 C CNN
F 3 "" H 12500 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 12500 2075 50  0001 C CNN "MFPN"
	1    12500 2075
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_7
U 1 1 6165C25C
P 13850 2075
F 0 "H2_7" H 13875 3500 60  0000 C CNN
F 1 "HEADER_2x26" H 13900 3400 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 13850 3466 60  0001 C CNN
F 3 "" H 13850 3325 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 13850 2075 50  0001 C CNN "MFPN"
	1    13850 2075
	1    0    0    -1  
$EndComp
Text GLabel 12225 875  0    50   BiDi ~ 0
H1_01
Text GLabel 12875 875  2    50   BiDi ~ 0
H1_02
Text GLabel 12225 975  0    50   BiDi ~ 0
H1_03
Text GLabel 12875 975  2    50   BiDi ~ 0
H1_04
Wire Wire Line
	12225 875  12300 875 
Wire Wire Line
	12225 975  12300 975 
Text GLabel 12225 1075 0    50   BiDi ~ 0
H1_05
Text GLabel 12225 1175 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	12225 1075 12300 1075
Wire Wire Line
	12225 1175 12300 1175
Text GLabel 12225 1275 0    50   BiDi ~ 0
H1_09
Text GLabel 12225 1375 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	12225 1275 12300 1275
Wire Wire Line
	12225 1375 12300 1375
Text GLabel 12225 1475 0    50   BiDi ~ 0
H1_13
Text GLabel 12225 1575 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	12225 1475 12300 1475
Wire Wire Line
	12225 1575 12300 1575
Text GLabel 12225 1675 0    50   BiDi ~ 0
H1_17
Text GLabel 12225 1775 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	12225 1675 12300 1675
Wire Wire Line
	12225 1775 12300 1775
Text GLabel 12225 1875 0    50   BiDi ~ 0
H1_21
Text GLabel 12225 1975 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	12225 1875 12300 1875
Wire Wire Line
	12225 1975 12300 1975
Text GLabel 12225 2075 0    50   BiDi ~ 0
H1_25
Text GLabel 12225 2175 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	12225 2075 12300 2075
Wire Wire Line
	12225 2175 12300 2175
Text GLabel 12225 2275 0    50   BiDi ~ 0
H1_29
Text GLabel 12225 2375 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	12225 2275 12300 2275
Wire Wire Line
	12225 2375 12300 2375
Text GLabel 12225 2475 0    50   BiDi ~ 0
H1_33
Text GLabel 12225 2575 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	12225 2475 12300 2475
Wire Wire Line
	12225 2575 12300 2575
Text GLabel 12225 2675 0    50   BiDi ~ 0
H1_37
Text GLabel 12225 2775 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	12225 2675 12300 2675
Wire Wire Line
	12225 2775 12300 2775
Text GLabel 12225 2875 0    50   BiDi ~ 0
H1_41
Text GLabel 12225 2975 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	12225 2875 12300 2875
Wire Wire Line
	12225 2975 12300 2975
Text GLabel 12225 3075 0    50   BiDi ~ 0
H1_45
Wire Wire Line
	12225 3075 12300 3075
Wire Wire Line
	12225 3175 12300 3175
Wire Wire Line
	12225 3275 12300 3275
Wire Wire Line
	12225 3375 12300 3375
Wire Wire Line
	12800 875  12875 875 
Wire Wire Line
	12800 975  12875 975 
Text GLabel 12875 1075 2    50   BiDi ~ 0
H1_06
Text GLabel 12875 1175 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	12800 1075 12875 1075
Wire Wire Line
	12800 1175 12875 1175
Text GLabel 12875 1275 2    50   BiDi ~ 0
H1_10
Text GLabel 12875 1375 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	12800 1275 12875 1275
Wire Wire Line
	12800 1375 12875 1375
Text GLabel 12875 1475 2    50   BiDi ~ 0
H1_14
Text GLabel 12875 1575 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	12800 1475 12875 1475
Wire Wire Line
	12800 1575 12875 1575
Text GLabel 12875 1675 2    50   BiDi ~ 0
H1_18
Text GLabel 12875 1775 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	12800 1675 12875 1675
Wire Wire Line
	12800 1775 12875 1775
Text GLabel 12875 1875 2    50   BiDi ~ 0
H1_22
Text GLabel 12875 1975 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	12800 1875 12875 1875
Wire Wire Line
	12800 1975 12875 1975
Text GLabel 12875 2075 2    50   BiDi ~ 0
H1_26
Text GLabel 12875 2175 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	12800 2075 12875 2075
Wire Wire Line
	12800 2175 12875 2175
Text GLabel 12875 2275 2    50   BiDi ~ 0
H1_30
Text GLabel 12875 2375 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	12800 2275 12875 2275
Wire Wire Line
	12800 2375 12875 2375
Text GLabel 12875 2475 2    50   BiDi ~ 0
H1_34
Text GLabel 12875 2575 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	12800 2475 12875 2475
Wire Wire Line
	12800 2575 12875 2575
Text GLabel 12875 2675 2    50   BiDi ~ 0
H1_38
Text GLabel 12875 2775 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	12800 2675 12875 2675
Wire Wire Line
	12800 2775 12875 2775
Text GLabel 12875 2875 2    50   BiDi ~ 0
H1_42
Text GLabel 12875 2975 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	12800 2875 12875 2875
Wire Wire Line
	12800 2975 12875 2975
Text GLabel 12875 3075 2    50   BiDi ~ 0
H1_46
Wire Wire Line
	12800 3075 12875 3075
Wire Wire Line
	12800 3175 12875 3175
Wire Wire Line
	12800 3275 12875 3275
Wire Wire Line
	12800 3375 12875 3375
Text GLabel 13575 875  0    50   BiDi ~ 0
H2_01
Text GLabel 13575 975  0    50   BiDi ~ 0
H2_03
Wire Wire Line
	13575 875  13650 875 
Wire Wire Line
	13575 975  13650 975 
Text GLabel 13575 1075 0    50   BiDi ~ 0
H2_05
Text GLabel 13575 1175 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	13575 1075 13650 1075
Wire Wire Line
	13575 1175 13650 1175
Text GLabel 13575 1275 0    50   BiDi ~ 0
H2_09
Text GLabel 13575 1375 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	13575 1275 13650 1275
Wire Wire Line
	13575 1375 13650 1375
Text GLabel 13575 1475 0    50   BiDi ~ 0
H2_13
Text GLabel 13575 1575 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	13575 1475 13650 1475
Wire Wire Line
	13575 1575 13650 1575
Text GLabel 13575 1675 0    50   BiDi ~ 0
H2_17
Text GLabel 13575 1775 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	13575 1675 13650 1675
Wire Wire Line
	13575 1775 13650 1775
Text GLabel 13575 1875 0    50   BiDi ~ 0
H2_21
Text GLabel 13575 1975 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	13575 1875 13650 1875
Wire Wire Line
	13575 1975 13650 1975
Wire Wire Line
	13575 2075 13650 2075
Wire Wire Line
	13575 2175 13650 2175
Wire Wire Line
	13575 2275 13650 2275
Wire Wire Line
	13575 2375 13650 2375
Text GLabel 13575 2475 0    50   BiDi ~ 0
H2_33
Text GLabel 13575 2575 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	13575 2475 13650 2475
Wire Wire Line
	13575 2575 13650 2575
Text GLabel 13575 2675 0    50   BiDi ~ 0
H2_37
Text GLabel 13575 2775 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	13575 2675 13650 2675
Wire Wire Line
	13575 2775 13650 2775
Text GLabel 13575 2875 0    50   BiDi ~ 0
H2_41
Text GLabel 13575 2975 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	13575 2875 13650 2875
Wire Wire Line
	13575 2975 13650 2975
Text GLabel 13575 3075 0    50   BiDi ~ 10
V_BATT
Text GLabel 13575 3175 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	13575 3075 13650 3075
Wire Wire Line
	13575 3175 13650 3175
Text GLabel 13575 3275 0    50   BiDi ~ 0
H2_49
Text GLabel 13575 3375 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	13575 3275 13650 3275
Wire Wire Line
	13575 3375 13650 3375
Text GLabel 14225 875  2    50   BiDi ~ 0
H2_02
Text GLabel 14225 975  2    50   BiDi ~ 0
H2_04
Wire Wire Line
	14150 875  14225 875 
Wire Wire Line
	14150 975  14225 975 
Text GLabel 14225 1075 2    50   BiDi ~ 0
H2_06
Text GLabel 14225 1175 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	14150 1075 14225 1075
Wire Wire Line
	14150 1175 14225 1175
Text GLabel 14225 1275 2    50   BiDi ~ 0
H2_10
Text GLabel 14225 1375 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	14150 1275 14225 1275
Wire Wire Line
	14150 1375 14225 1375
Text GLabel 14225 1475 2    50   BiDi ~ 0
H2_14
Text GLabel 14225 1575 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	14150 1475 14225 1475
Wire Wire Line
	14150 1575 14225 1575
Text GLabel 14225 1675 2    50   BiDi ~ 0
H2_18
Text GLabel 14225 1775 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	14150 1675 14225 1675
Wire Wire Line
	14150 1775 14225 1775
Text GLabel 14225 1875 2    50   BiDi ~ 0
H2_22
Text GLabel 14225 1975 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	14150 1875 14225 1875
Wire Wire Line
	14150 1975 14225 1975
Wire Wire Line
	14150 2075 14225 2075
Wire Wire Line
	14150 2175 14225 2175
Wire Wire Line
	14150 2275 14225 2275
Wire Wire Line
	14150 2375 14225 2375
Text GLabel 14225 2475 2    50   BiDi ~ 0
H2_34
Text GLabel 14225 2575 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	14150 2475 14225 2475
Wire Wire Line
	14150 2575 14225 2575
Text GLabel 14225 2675 2    50   BiDi ~ 0
H2_38
Text GLabel 14225 2775 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	14150 2675 14225 2675
Wire Wire Line
	14150 2775 14225 2775
Text GLabel 14225 2875 2    50   BiDi ~ 0
H2_42
Text GLabel 14225 2975 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	14150 2875 14225 2875
Wire Wire Line
	14150 2975 14225 2975
Text GLabel 14225 3075 2    50   BiDi ~ 10
V_BATT
Text GLabel 14225 3175 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	14150 3075 14225 3075
Wire Wire Line
	14150 3175 14225 3175
Text GLabel 14225 3275 2    50   BiDi ~ 0
H2_50
Text GLabel 14225 3375 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	14150 3275 14225 3275
Wire Wire Line
	14150 3375 14225 3375
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_8
U 1 1 6165C34B
P 12500 5050
F 0 "H1_8" H 12525 6475 60  0000 C CNN
F 1 "HEADER_2x26" H 12550 6375 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 12500 6441 60  0001 C CNN
F 3 "" H 12500 6300 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 12500 5050 50  0001 C CNN "MFPN"
	1    12500 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_8
U 1 1 6165C351
P 13850 5050
F 0 "H2_8" H 13900 6475 60  0000 C CNN
F 1 "HEADER_2x26" H 13900 6375 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 13850 6441 60  0001 C CNN
F 3 "" H 13850 6300 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 13850 5050 50  0001 C CNN "MFPN"
	1    13850 5050
	1    0    0    -1  
$EndComp
Text GLabel 12225 3850 0    50   BiDi ~ 0
H1_01
Text GLabel 12875 3850 2    50   BiDi ~ 0
H1_02
Text GLabel 12225 3950 0    50   BiDi ~ 0
H1_03
Text GLabel 12875 3950 2    50   BiDi ~ 0
H1_04
Wire Wire Line
	12225 3850 12300 3850
Wire Wire Line
	12225 3950 12300 3950
Text GLabel 12225 4050 0    50   BiDi ~ 0
H1_05
Text GLabel 12225 4150 0    50   BiDi ~ 0
H1_07
Wire Wire Line
	12225 4050 12300 4050
Wire Wire Line
	12225 4150 12300 4150
Text GLabel 12225 4250 0    50   BiDi ~ 0
H1_09
Text GLabel 12225 4350 0    50   BiDi ~ 0
H1_11
Wire Wire Line
	12225 4250 12300 4250
Wire Wire Line
	12225 4350 12300 4350
Text GLabel 12225 4450 0    50   BiDi ~ 0
H1_13
Text GLabel 12225 4550 0    50   BiDi ~ 0
H1_15
Wire Wire Line
	12225 4450 12300 4450
Wire Wire Line
	12225 4550 12300 4550
Text GLabel 12225 4650 0    50   BiDi ~ 0
H1_17
Text GLabel 12225 4750 0    50   BiDi ~ 0
H1_19
Wire Wire Line
	12225 4650 12300 4650
Wire Wire Line
	12225 4750 12300 4750
Text GLabel 12225 4850 0    50   BiDi ~ 0
H1_21
Text GLabel 12225 4950 0    50   BiDi ~ 0
H1_23
Wire Wire Line
	12225 4850 12300 4850
Wire Wire Line
	12225 4950 12300 4950
Text GLabel 12225 5050 0    50   BiDi ~ 0
H1_25
Text GLabel 12225 5150 0    50   BiDi ~ 0
H1_27
Wire Wire Line
	12225 5050 12300 5050
Wire Wire Line
	12225 5150 12300 5150
Text GLabel 12225 5250 0    50   BiDi ~ 0
H1_29
Text GLabel 12225 5350 0    50   BiDi ~ 0
H1_31
Wire Wire Line
	12225 5250 12300 5250
Wire Wire Line
	12225 5350 12300 5350
Text GLabel 12225 5450 0    50   BiDi ~ 0
H1_33
Text GLabel 12225 5550 0    50   BiDi ~ 0
H1_35
Wire Wire Line
	12225 5450 12300 5450
Wire Wire Line
	12225 5550 12300 5550
Text GLabel 12225 5650 0    50   BiDi ~ 0
H1_37
Text GLabel 12225 5750 0    50   BiDi ~ 0
H1_39
Wire Wire Line
	12225 5650 12300 5650
Wire Wire Line
	12225 5750 12300 5750
Text GLabel 12225 5850 0    50   BiDi ~ 0
H1_41
Text GLabel 12225 5950 0    50   BiDi ~ 0
H1_43
Wire Wire Line
	12225 5850 12300 5850
Wire Wire Line
	12225 5950 12300 5950
Text GLabel 12225 6050 0    50   BiDi ~ 0
H1_45
Wire Wire Line
	12225 6050 12300 6050
Wire Wire Line
	12225 6150 12300 6150
Wire Wire Line
	12225 6250 12300 6250
Wire Wire Line
	12225 6350 12300 6350
Wire Wire Line
	12800 3850 12875 3850
Wire Wire Line
	12800 3950 12875 3950
Text GLabel 12875 4050 2    50   BiDi ~ 0
H1_06
Text GLabel 12875 4150 2    50   BiDi ~ 0
H1_08
Wire Wire Line
	12800 4050 12875 4050
Wire Wire Line
	12800 4150 12875 4150
Text GLabel 12875 4250 2    50   BiDi ~ 0
H1_10
Text GLabel 12875 4350 2    50   BiDi ~ 0
H1_12
Wire Wire Line
	12800 4250 12875 4250
Wire Wire Line
	12800 4350 12875 4350
Text GLabel 12875 4450 2    50   BiDi ~ 0
H1_14
Text GLabel 12875 4550 2    50   BiDi ~ 0
H1_16
Wire Wire Line
	12800 4450 12875 4450
Wire Wire Line
	12800 4550 12875 4550
Text GLabel 12875 4650 2    50   BiDi ~ 0
H1_18
Text GLabel 12875 4750 2    50   BiDi ~ 0
H1_20
Wire Wire Line
	12800 4650 12875 4650
Wire Wire Line
	12800 4750 12875 4750
Text GLabel 12875 4850 2    50   BiDi ~ 0
H1_22
Text GLabel 12875 4950 2    50   BiDi ~ 0
H1_24
Wire Wire Line
	12800 4850 12875 4850
Wire Wire Line
	12800 4950 12875 4950
Text GLabel 12875 5050 2    50   BiDi ~ 0
H1_26
Text GLabel 12875 5150 2    50   BiDi ~ 0
H1_28
Wire Wire Line
	12800 5050 12875 5050
Wire Wire Line
	12800 5150 12875 5150
Text GLabel 12875 5250 2    50   BiDi ~ 0
H1_30
Text GLabel 12875 5350 2    50   BiDi ~ 10
CH_5V
Wire Wire Line
	12800 5250 12875 5250
Wire Wire Line
	12800 5350 12875 5350
Text GLabel 12875 5450 2    50   BiDi ~ 0
H1_34
Text GLabel 12875 5550 2    50   BiDi ~ 0
H1_36
Wire Wire Line
	12800 5450 12875 5450
Wire Wire Line
	12800 5550 12875 5550
Text GLabel 12875 5650 2    50   BiDi ~ 0
H1_38
Text GLabel 12875 5750 2    50   BiDi ~ 0
H1_40
Wire Wire Line
	12800 5650 12875 5650
Wire Wire Line
	12800 5750 12875 5750
Text GLabel 12875 5850 2    50   BiDi ~ 0
H1_42
Text GLabel 12875 5950 2    50   BiDi ~ 0
H1_44
Wire Wire Line
	12800 5850 12875 5850
Wire Wire Line
	12800 5950 12875 5950
Text GLabel 12875 6050 2    50   BiDi ~ 0
H1_46
Wire Wire Line
	12800 6050 12875 6050
Wire Wire Line
	12800 6150 12875 6150
Wire Wire Line
	12800 6250 12875 6250
Wire Wire Line
	12800 6350 12875 6350
Text GLabel 13575 3850 0    50   BiDi ~ 0
H2_01
Text GLabel 13575 3950 0    50   BiDi ~ 0
H2_03
Wire Wire Line
	13575 3850 13650 3850
Wire Wire Line
	13575 3950 13650 3950
Text GLabel 13575 4050 0    50   BiDi ~ 0
H2_05
Text GLabel 13575 4150 0    50   BiDi ~ 0
H2_07
Wire Wire Line
	13575 4050 13650 4050
Wire Wire Line
	13575 4150 13650 4150
Text GLabel 13575 4250 0    50   BiDi ~ 0
H2_09
Text GLabel 13575 4350 0    50   BiDi ~ 0
H2_11
Wire Wire Line
	13575 4250 13650 4250
Wire Wire Line
	13575 4350 13650 4350
Text GLabel 13575 4450 0    50   BiDi ~ 0
H2_13
Text GLabel 13575 4550 0    50   BiDi ~ 0
H2_15
Wire Wire Line
	13575 4450 13650 4450
Wire Wire Line
	13575 4550 13650 4550
Text GLabel 13575 4650 0    50   BiDi ~ 0
H2_17
Text GLabel 13575 4750 0    50   BiDi ~ 0
H2_19
Wire Wire Line
	13575 4650 13650 4650
Wire Wire Line
	13575 4750 13650 4750
Text GLabel 13575 4850 0    50   BiDi ~ 0
H2_21
Text GLabel 13575 4950 0    50   BiDi ~ 0
H2_23
Wire Wire Line
	13575 4850 13650 4850
Wire Wire Line
	13575 4950 13650 4950
Wire Wire Line
	13575 5050 13650 5050
Wire Wire Line
	13575 5150 13650 5150
Wire Wire Line
	13575 5250 13650 5250
Wire Wire Line
	13575 5350 13650 5350
Text GLabel 13575 5450 0    50   BiDi ~ 0
H2_33
Text GLabel 13575 5550 0    50   BiDi ~ 0
H2_35
Wire Wire Line
	13575 5450 13650 5450
Wire Wire Line
	13575 5550 13650 5550
Text GLabel 13575 5650 0    50   BiDi ~ 0
H2_37
Text GLabel 13575 5750 0    50   BiDi ~ 0
H2_39
Wire Wire Line
	13575 5650 13650 5650
Wire Wire Line
	13575 5750 13650 5750
Text GLabel 13575 5850 0    50   BiDi ~ 0
H2_41
Text GLabel 13575 5950 0    50   BiDi ~ 0
H2_43
Wire Wire Line
	13575 5850 13650 5850
Wire Wire Line
	13575 5950 13650 5950
Text GLabel 13575 6050 0    50   BiDi ~ 10
V_BATT
Text GLabel 13575 6150 0    50   BiDi ~ 0
H2_47
Wire Wire Line
	13575 6050 13650 6050
Wire Wire Line
	13575 6150 13650 6150
Text GLabel 13575 6250 0    50   BiDi ~ 0
H2_49
Text GLabel 13575 6350 0    50   BiDi ~ 0
H2_51
Wire Wire Line
	13575 6250 13650 6250
Wire Wire Line
	13575 6350 13650 6350
Text GLabel 14225 3850 2    50   BiDi ~ 0
H2_02
Text GLabel 14225 3950 2    50   BiDi ~ 0
H2_04
Wire Wire Line
	14150 3850 14225 3850
Wire Wire Line
	14150 3950 14225 3950
Text GLabel 14225 4050 2    50   BiDi ~ 0
H2_06
Text GLabel 14225 4150 2    50   BiDi ~ 0
H2_08
Wire Wire Line
	14150 4050 14225 4050
Wire Wire Line
	14150 4150 14225 4150
Text GLabel 14225 4250 2    50   BiDi ~ 0
H2_10
Text GLabel 14225 4350 2    50   BiDi ~ 0
H2_12
Wire Wire Line
	14150 4250 14225 4250
Wire Wire Line
	14150 4350 14225 4350
Text GLabel 14225 4450 2    50   BiDi ~ 0
H2_14
Text GLabel 14225 4550 2    50   BiDi ~ 0
H2_16
Wire Wire Line
	14150 4450 14225 4450
Wire Wire Line
	14150 4550 14225 4550
Text GLabel 14225 4650 2    50   BiDi ~ 0
H2_18
Text GLabel 14225 4750 2    50   BiDi ~ 0
H2_20
Wire Wire Line
	14150 4650 14225 4650
Wire Wire Line
	14150 4750 14225 4750
Text GLabel 14225 4850 2    50   BiDi ~ 0
H2_22
Text GLabel 14225 4950 2    50   BiDi ~ 0
H2_24
Wire Wire Line
	14150 4850 14225 4850
Wire Wire Line
	14150 4950 14225 4950
Wire Wire Line
	14150 5050 14225 5050
Wire Wire Line
	14150 5150 14225 5150
Wire Wire Line
	14150 5250 14225 5250
Wire Wire Line
	14150 5350 14225 5350
Text GLabel 14225 5450 2    50   BiDi ~ 0
H2_34
Text GLabel 14225 5550 2    50   BiDi ~ 0
H2_36
Wire Wire Line
	14150 5450 14225 5450
Wire Wire Line
	14150 5550 14225 5550
Text GLabel 14225 5650 2    50   BiDi ~ 0
H2_38
Text GLabel 14225 5750 2    50   BiDi ~ 0
H2_40
Wire Wire Line
	14150 5650 14225 5650
Wire Wire Line
	14150 5750 14225 5750
Text GLabel 14225 5850 2    50   BiDi ~ 0
H2_42
Text GLabel 14225 5950 2    50   BiDi ~ 0
H2_44
Wire Wire Line
	14150 5850 14225 5850
Wire Wire Line
	14150 5950 14225 5950
Text GLabel 14225 6050 2    50   BiDi ~ 10
V_BATT
Text GLabel 14225 6150 2    50   BiDi ~ 0
H2_48
Wire Wire Line
	14150 6050 14225 6050
Wire Wire Line
	14150 6150 14225 6150
Text GLabel 14225 6250 2    50   BiDi ~ 0
H2_50
Text GLabel 14225 6350 2    50   BiDi ~ 0
H2_52
Wire Wire Line
	14150 6250 14225 6250
Wire Wire Line
	14150 6350 14225 6350
Wire Notes Line
	11200 550  14625 550 
Wire Notes Line
	11200 550  11200 6500
Wire Notes Line
	11200 3525 14625 3525
Wire Notes Line
	14625 550  14625 6500
Wire Notes Line
	11200 6500 14625 6500
$Comp
L power:GND #PWR0101
U 1 1 633079E1
P 3725 2275
F 0 "#PWR0101" H 3725 2025 50  0001 C CNN
F 1 "GND" V 3730 2147 50  0000 R CNN
F 2 "" H 3725 2275 50  0001 C CNN
F 3 "" H 3725 2275 50  0001 C CNN
	1    3725 2275
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6330974E
P 3075 2275
F 0 "#PWR0102" H 3075 2025 50  0001 C CNN
F 1 "GND" V 3080 2147 50  0000 R CNN
F 2 "" H 3075 2275 50  0001 C CNN
F 3 "" H 3075 2275 50  0001 C CNN
	1    3075 2275
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 6330B21B
P 3725 2375
F 0 "#PWR0103" H 3725 2125 50  0001 C CNN
F 1 "GND" V 3730 2247 50  0000 R CNN
F 2 "" H 3725 2375 50  0001 C CNN
F 3 "" H 3725 2375 50  0001 C CNN
	1    3725 2375
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDA #PWR0104
U 1 1 6330CCF4
P 3075 2375
F 0 "#PWR0104" H 3075 2125 50  0001 C CNN
F 1 "GNDA" V 3080 2248 50  0000 R CNN
F 2 "" H 3075 2375 50  0001 C CNN
F 3 "" H 3075 2375 50  0001 C CNN
	1    3075 2375
	0    1    1    0   
$EndComp
Text GLabel 3725 3075 2    50   BiDi ~ 10
V_BATT
Text GLabel 3075 3075 0    50   BiDi ~ 10
V_BATT
$Comp
L power:GND #PWR0105
U 1 1 6337263B
P 7150 2275
F 0 "#PWR0105" H 7150 2025 50  0001 C CNN
F 1 "GND" V 7155 2147 50  0000 R CNN
F 2 "" H 7150 2275 50  0001 C CNN
F 3 "" H 7150 2275 50  0001 C CNN
	1    7150 2275
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 63372641
P 7150 2375
F 0 "#PWR0106" H 7150 2125 50  0001 C CNN
F 1 "GND" V 7155 2247 50  0000 R CNN
F 2 "" H 7150 2375 50  0001 C CNN
F 3 "" H 7150 2375 50  0001 C CNN
	1    7150 2375
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 63499205
P 10575 2275
F 0 "#PWR0107" H 10575 2025 50  0001 C CNN
F 1 "GND" V 10580 2147 50  0000 R CNN
F 2 "" H 10575 2275 50  0001 C CNN
F 3 "" H 10575 2275 50  0001 C CNN
	1    10575 2275
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 6349920B
P 10575 2375
F 0 "#PWR0108" H 10575 2125 50  0001 C CNN
F 1 "GND" V 10580 2247 50  0000 R CNN
F 2 "" H 10575 2375 50  0001 C CNN
F 3 "" H 10575 2375 50  0001 C CNN
	1    10575 2375
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 635BCF27
P 14225 2275
F 0 "#PWR0109" H 14225 2025 50  0001 C CNN
F 1 "GND" V 14230 2147 50  0000 R CNN
F 2 "" H 14225 2275 50  0001 C CNN
F 3 "" H 14225 2275 50  0001 C CNN
	1    14225 2275
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 635BCF2D
P 14225 2375
F 0 "#PWR0110" H 14225 2125 50  0001 C CNN
F 1 "GND" V 14230 2247 50  0000 R CNN
F 2 "" H 14225 2375 50  0001 C CNN
F 3 "" H 14225 2375 50  0001 C CNN
	1    14225 2375
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 636E1A63
P 14225 5250
F 0 "#PWR0111" H 14225 5000 50  0001 C CNN
F 1 "GND" V 14230 5122 50  0000 R CNN
F 2 "" H 14225 5250 50  0001 C CNN
F 3 "" H 14225 5250 50  0001 C CNN
	1    14225 5250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 636E1A69
P 14225 5350
F 0 "#PWR0112" H 14225 5100 50  0001 C CNN
F 1 "GND" V 14230 5222 50  0000 R CNN
F 2 "" H 14225 5350 50  0001 C CNN
F 3 "" H 14225 5350 50  0001 C CNN
	1    14225 5350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 63807367
P 10575 5250
F 0 "#PWR0113" H 10575 5000 50  0001 C CNN
F 1 "GND" V 10580 5122 50  0000 R CNN
F 2 "" H 10575 5250 50  0001 C CNN
F 3 "" H 10575 5250 50  0001 C CNN
	1    10575 5250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 6380736D
P 10575 5350
F 0 "#PWR0114" H 10575 5100 50  0001 C CNN
F 1 "GND" V 10580 5222 50  0000 R CNN
F 2 "" H 10575 5350 50  0001 C CNN
F 3 "" H 10575 5350 50  0001 C CNN
	1    10575 5350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 6392DA7D
P 7150 5250
F 0 "#PWR0115" H 7150 5000 50  0001 C CNN
F 1 "GND" V 7155 5122 50  0000 R CNN
F 2 "" H 7150 5250 50  0001 C CNN
F 3 "" H 7150 5250 50  0001 C CNN
	1    7150 5250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 6392DA83
P 7150 5350
F 0 "#PWR0116" H 7150 5100 50  0001 C CNN
F 1 "GND" V 7155 5222 50  0000 R CNN
F 2 "" H 7150 5350 50  0001 C CNN
F 3 "" H 7150 5350 50  0001 C CNN
	1    7150 5350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 63A52ECB
P 3725 5250
F 0 "#PWR0117" H 3725 5000 50  0001 C CNN
F 1 "GND" V 3730 5122 50  0000 R CNN
F 2 "" H 3725 5250 50  0001 C CNN
F 3 "" H 3725 5250 50  0001 C CNN
	1    3725 5250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 63A52ED1
P 3725 5350
F 0 "#PWR0118" H 3725 5100 50  0001 C CNN
F 1 "GND" V 3730 5222 50  0000 R CNN
F 2 "" H 3725 5350 50  0001 C CNN
F 3 "" H 3725 5350 50  0001 C CNN
	1    3725 5350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 63CA71B3
P 3075 5250
F 0 "#PWR0119" H 3075 5000 50  0001 C CNN
F 1 "GND" V 3080 5122 50  0000 R CNN
F 2 "" H 3075 5250 50  0001 C CNN
F 3 "" H 3075 5250 50  0001 C CNN
	1    3075 5250
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0120
U 1 1 63CA71B9
P 3075 5350
F 0 "#PWR0120" H 3075 5100 50  0001 C CNN
F 1 "GNDA" V 3080 5223 50  0000 R CNN
F 2 "" H 3075 5350 50  0001 C CNN
F 3 "" H 3075 5350 50  0001 C CNN
	1    3075 5350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 63DCCE21
P 6500 5250
F 0 "#PWR0121" H 6500 5000 50  0001 C CNN
F 1 "GND" V 6505 5122 50  0000 R CNN
F 2 "" H 6500 5250 50  0001 C CNN
F 3 "" H 6500 5250 50  0001 C CNN
	1    6500 5250
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0122
U 1 1 63DCCE27
P 6500 5350
F 0 "#PWR0122" H 6500 5100 50  0001 C CNN
F 1 "GNDA" V 6505 5223 50  0000 R CNN
F 2 "" H 6500 5350 50  0001 C CNN
F 3 "" H 6500 5350 50  0001 C CNN
	1    6500 5350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 63EF2FBF
P 9925 5250
F 0 "#PWR0123" H 9925 5000 50  0001 C CNN
F 1 "GND" V 9930 5122 50  0000 R CNN
F 2 "" H 9925 5250 50  0001 C CNN
F 3 "" H 9925 5250 50  0001 C CNN
	1    9925 5250
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0124
U 1 1 63EF2FC5
P 9925 5350
F 0 "#PWR0124" H 9925 5100 50  0001 C CNN
F 1 "GNDA" V 9930 5223 50  0000 R CNN
F 2 "" H 9925 5350 50  0001 C CNN
F 3 "" H 9925 5350 50  0001 C CNN
	1    9925 5350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 6401877B
P 13575 5250
F 0 "#PWR0125" H 13575 5000 50  0001 C CNN
F 1 "GND" V 13580 5122 50  0000 R CNN
F 2 "" H 13575 5250 50  0001 C CNN
F 3 "" H 13575 5250 50  0001 C CNN
	1    13575 5250
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0126
U 1 1 64018781
P 13575 5350
F 0 "#PWR0126" H 13575 5100 50  0001 C CNN
F 1 "GNDA" V 13580 5223 50  0000 R CNN
F 2 "" H 13575 5350 50  0001 C CNN
F 3 "" H 13575 5350 50  0001 C CNN
	1    13575 5350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 6413C6F1
P 13575 2275
F 0 "#PWR0127" H 13575 2025 50  0001 C CNN
F 1 "GND" V 13580 2147 50  0000 R CNN
F 2 "" H 13575 2275 50  0001 C CNN
F 3 "" H 13575 2275 50  0001 C CNN
	1    13575 2275
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0128
U 1 1 6413C6F7
P 13575 2375
F 0 "#PWR0128" H 13575 2125 50  0001 C CNN
F 1 "GNDA" V 13580 2248 50  0000 R CNN
F 2 "" H 13575 2375 50  0001 C CNN
F 3 "" H 13575 2375 50  0001 C CNN
	1    13575 2375
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 64260D73
P 9925 2275
F 0 "#PWR0129" H 9925 2025 50  0001 C CNN
F 1 "GND" V 9930 2147 50  0000 R CNN
F 2 "" H 9925 2275 50  0001 C CNN
F 3 "" H 9925 2275 50  0001 C CNN
	1    9925 2275
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0130
U 1 1 64260D79
P 9925 2375
F 0 "#PWR0130" H 9925 2125 50  0001 C CNN
F 1 "GNDA" V 9930 2248 50  0000 R CNN
F 2 "" H 9925 2375 50  0001 C CNN
F 3 "" H 9925 2375 50  0001 C CNN
	1    9925 2375
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0131
U 1 1 64384FB5
P 6500 2275
F 0 "#PWR0131" H 6500 2025 50  0001 C CNN
F 1 "GND" V 6505 2147 50  0000 R CNN
F 2 "" H 6500 2275 50  0001 C CNN
F 3 "" H 6500 2275 50  0001 C CNN
	1    6500 2275
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0132
U 1 1 64384FBB
P 6500 2375
F 0 "#PWR0132" H 6500 2125 50  0001 C CNN
F 1 "GNDA" V 6505 2248 50  0000 R CNN
F 2 "" H 6500 2375 50  0001 C CNN
F 3 "" H 6500 2375 50  0001 C CNN
	1    6500 2375
	0    1    1    0   
$EndComp
Text GLabel 1725 6150 0    50   BiDi ~ 10
LU_1
Text GLabel 1725 6250 0    50   BiDi ~ 10
LU_2
Text GLabel 1725 6350 0    50   BiDi ~ 10
LU_3
Text GLabel 5150 6150 0    50   BiDi ~ 10
LU_1
Text GLabel 5150 6250 0    50   BiDi ~ 10
LU_2
Text GLabel 5150 6350 0    50   BiDi ~ 10
LU_3
Text GLabel 8575 6150 0    50   BiDi ~ 10
LU_1
Text GLabel 8575 6250 0    50   BiDi ~ 10
LU_2
Text GLabel 8575 6350 0    50   BiDi ~ 10
LU_3
Text GLabel 12225 3175 0    50   BiDi ~ 10
LU_1
Text GLabel 12225 3275 0    50   BiDi ~ 10
LU_2
Text GLabel 12225 3375 0    50   BiDi ~ 10
LU_3
Text GLabel 8575 3175 0    50   BiDi ~ 10
LU_1
Text GLabel 8575 3275 0    50   BiDi ~ 10
LU_2
Text GLabel 8575 3375 0    50   BiDi ~ 10
LU_3
Text GLabel 5150 3175 0    50   BiDi ~ 10
LU_1
Text GLabel 5150 3275 0    50   BiDi ~ 10
LU_2
Text GLabel 5150 3375 0    50   BiDi ~ 10
LU_3
Text GLabel 5800 3175 2    50   BiDi ~ 10
LU_4
Text GLabel 5800 3275 2    50   BiDi ~ 10
LU_5
Text GLabel 5800 3375 2    50   BiDi ~ 10
LU_6
Text GLabel 9225 3175 2    50   BiDi ~ 10
LU_4
Text GLabel 9225 3275 2    50   BiDi ~ 10
LU_5
Text GLabel 9225 3375 2    50   BiDi ~ 10
LU_6
Text GLabel 12875 3175 2    50   BiDi ~ 10
LU_4
Text GLabel 12875 3275 2    50   BiDi ~ 10
LU_5
Text GLabel 12875 3375 2    50   BiDi ~ 10
LU_6
Text GLabel 12875 6150 2    50   BiDi ~ 10
LU_4
Text GLabel 12875 6250 2    50   BiDi ~ 10
LU_5
Text GLabel 12875 6350 2    50   BiDi ~ 10
LU_6
Text GLabel 9225 6150 2    50   BiDi ~ 10
LU_4
Text GLabel 9225 6250 2    50   BiDi ~ 10
LU_5
Text GLabel 9225 6350 2    50   BiDi ~ 10
LU_6
Text GLabel 5800 6150 2    50   BiDi ~ 10
LU_4
Text GLabel 5800 6250 2    50   BiDi ~ 10
LU_5
Text GLabel 5800 6350 2    50   BiDi ~ 10
LU_6
Text GLabel 2375 6150 2    50   BiDi ~ 10
LU_4
Text GLabel 2375 6250 2    50   BiDi ~ 10
LU_5
Text GLabel 2375 6350 2    50   BiDi ~ 10
LU_6
Text GLabel 12225 6150 0    50   BiDi ~ 10
LU_1
Text GLabel 12225 6250 0    50   BiDi ~ 10
LU_2
Text GLabel 12225 6350 0    50   BiDi ~ 10
LU_3
Text GLabel 2375 2075 2    50   BiDi ~ 0
H1_26
Text GLabel 1725 2075 0    50   BiDi ~ 0
H1_25
$Comp
L power:+5V #PWR0133
U 1 1 66771852
P 3075 2075
F 0 "#PWR0133" H 3075 1925 50  0001 C CNN
F 1 "+5V" V 3090 2203 50  0000 L CNN
F 2 "" H 3075 2075 50  0001 C CNN
F 3 "" H 3075 2075 50  0001 C CNN
	1    3075 2075
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0134
U 1 1 667730FB
P 3725 2075
F 0 "#PWR0134" H 3725 1925 50  0001 C CNN
F 1 "+5V" V 3740 2203 50  0000 L CNN
F 2 "" H 3725 2075 50  0001 C CNN
F 3 "" H 3725 2075 50  0001 C CNN
	1    3725 2075
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0135
U 1 1 6679F6CF
P 3075 2175
F 0 "#PWR0135" H 3075 2025 50  0001 C CNN
F 1 "+3V3" V 3090 2303 50  0000 L CNN
F 2 "" H 3075 2175 50  0001 C CNN
F 3 "" H 3075 2175 50  0001 C CNN
	1    3075 2175
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0136
U 1 1 667A11B2
P 3725 2175
F 0 "#PWR0136" H 3725 2025 50  0001 C CNN
F 1 "+3V3" V 3740 2303 50  0000 L CNN
F 2 "" H 3725 2175 50  0001 C CNN
F 3 "" H 3725 2175 50  0001 C CNN
	1    3725 2175
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0137
U 1 1 667A7322
P 6500 2075
F 0 "#PWR0137" H 6500 1925 50  0001 C CNN
F 1 "+5V" V 6515 2203 50  0000 L CNN
F 2 "" H 6500 2075 50  0001 C CNN
F 3 "" H 6500 2075 50  0001 C CNN
	1    6500 2075
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0138
U 1 1 667A7328
P 6500 2175
F 0 "#PWR0138" H 6500 2025 50  0001 C CNN
F 1 "+3V3" V 6515 2303 50  0000 L CNN
F 2 "" H 6500 2175 50  0001 C CNN
F 3 "" H 6500 2175 50  0001 C CNN
	1    6500 2175
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0139
U 1 1 668CC85C
P 9925 2075
F 0 "#PWR0139" H 9925 1925 50  0001 C CNN
F 1 "+5V" V 9940 2203 50  0000 L CNN
F 2 "" H 9925 2075 50  0001 C CNN
F 3 "" H 9925 2075 50  0001 C CNN
	1    9925 2075
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0140
U 1 1 668CC862
P 9925 2175
F 0 "#PWR0140" H 9925 2025 50  0001 C CNN
F 1 "+3V3" V 9940 2303 50  0000 L CNN
F 2 "" H 9925 2175 50  0001 C CNN
F 3 "" H 9925 2175 50  0001 C CNN
	1    9925 2175
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0141
U 1 1 669F171A
P 13575 2075
F 0 "#PWR0141" H 13575 1925 50  0001 C CNN
F 1 "+5V" V 13590 2203 50  0000 L CNN
F 2 "" H 13575 2075 50  0001 C CNN
F 3 "" H 13575 2075 50  0001 C CNN
	1    13575 2075
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0142
U 1 1 669F1720
P 13575 2175
F 0 "#PWR0142" H 13575 2025 50  0001 C CNN
F 1 "+3V3" V 13590 2303 50  0000 L CNN
F 2 "" H 13575 2175 50  0001 C CNN
F 3 "" H 13575 2175 50  0001 C CNN
	1    13575 2175
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0143
U 1 1 66C37C5C
P 13575 5050
F 0 "#PWR0143" H 13575 4900 50  0001 C CNN
F 1 "+5V" V 13590 5178 50  0000 L CNN
F 2 "" H 13575 5050 50  0001 C CNN
F 3 "" H 13575 5050 50  0001 C CNN
	1    13575 5050
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0144
U 1 1 66C37C62
P 13575 5150
F 0 "#PWR0144" H 13575 5000 50  0001 C CNN
F 1 "+3V3" V 13590 5278 50  0000 L CNN
F 2 "" H 13575 5150 50  0001 C CNN
F 3 "" H 13575 5150 50  0001 C CNN
	1    13575 5150
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0145
U 1 1 66D5DD0E
P 9925 5050
F 0 "#PWR0145" H 9925 4900 50  0001 C CNN
F 1 "+5V" V 9940 5178 50  0000 L CNN
F 2 "" H 9925 5050 50  0001 C CNN
F 3 "" H 9925 5050 50  0001 C CNN
	1    9925 5050
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0146
U 1 1 66D5DD14
P 9925 5150
F 0 "#PWR0146" H 9925 5000 50  0001 C CNN
F 1 "+3V3" V 9940 5278 50  0000 L CNN
F 2 "" H 9925 5150 50  0001 C CNN
F 3 "" H 9925 5150 50  0001 C CNN
	1    9925 5150
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0147
U 1 1 66E83224
P 6500 5050
F 0 "#PWR0147" H 6500 4900 50  0001 C CNN
F 1 "+5V" V 6515 5178 50  0000 L CNN
F 2 "" H 6500 5050 50  0001 C CNN
F 3 "" H 6500 5050 50  0001 C CNN
	1    6500 5050
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0148
U 1 1 66E8322A
P 6500 5150
F 0 "#PWR0148" H 6500 5000 50  0001 C CNN
F 1 "+3V3" V 6515 5278 50  0000 L CNN
F 2 "" H 6500 5150 50  0001 C CNN
F 3 "" H 6500 5150 50  0001 C CNN
	1    6500 5150
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0149
U 1 1 66FA76F0
P 3075 5050
F 0 "#PWR0149" H 3075 4900 50  0001 C CNN
F 1 "+5V" V 3090 5178 50  0000 L CNN
F 2 "" H 3075 5050 50  0001 C CNN
F 3 "" H 3075 5050 50  0001 C CNN
	1    3075 5050
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0150
U 1 1 66FA76F6
P 3075 5150
F 0 "#PWR0150" H 3075 5000 50  0001 C CNN
F 1 "+3V3" V 3090 5278 50  0000 L CNN
F 2 "" H 3075 5150 50  0001 C CNN
F 3 "" H 3075 5150 50  0001 C CNN
	1    3075 5150
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0151
U 1 1 670D31E2
P 3725 5050
F 0 "#PWR0151" H 3725 4900 50  0001 C CNN
F 1 "+5V" V 3740 5178 50  0000 L CNN
F 2 "" H 3725 5050 50  0001 C CNN
F 3 "" H 3725 5050 50  0001 C CNN
	1    3725 5050
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0152
U 1 1 670D31E8
P 3725 5150
F 0 "#PWR0152" H 3725 5000 50  0001 C CNN
F 1 "+3V3" V 3740 5278 50  0000 L CNN
F 2 "" H 3725 5150 50  0001 C CNN
F 3 "" H 3725 5150 50  0001 C CNN
	1    3725 5150
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0153
U 1 1 6731B800
P 7150 5050
F 0 "#PWR0153" H 7150 4900 50  0001 C CNN
F 1 "+5V" V 7165 5178 50  0000 L CNN
F 2 "" H 7150 5050 50  0001 C CNN
F 3 "" H 7150 5050 50  0001 C CNN
	1    7150 5050
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0154
U 1 1 6731B806
P 7150 5150
F 0 "#PWR0154" H 7150 5000 50  0001 C CNN
F 1 "+3V3" V 7165 5278 50  0000 L CNN
F 2 "" H 7150 5150 50  0001 C CNN
F 3 "" H 7150 5150 50  0001 C CNN
	1    7150 5150
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0155
U 1 1 6743F890
P 10575 5050
F 0 "#PWR0155" H 10575 4900 50  0001 C CNN
F 1 "+5V" V 10590 5178 50  0000 L CNN
F 2 "" H 10575 5050 50  0001 C CNN
F 3 "" H 10575 5050 50  0001 C CNN
	1    10575 5050
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0156
U 1 1 6743F896
P 10575 5150
F 0 "#PWR0156" H 10575 5000 50  0001 C CNN
F 1 "+3V3" V 10590 5278 50  0000 L CNN
F 2 "" H 10575 5150 50  0001 C CNN
F 3 "" H 10575 5150 50  0001 C CNN
	1    10575 5150
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0157
U 1 1 67564566
P 14225 5050
F 0 "#PWR0157" H 14225 4900 50  0001 C CNN
F 1 "+5V" V 14240 5178 50  0000 L CNN
F 2 "" H 14225 5050 50  0001 C CNN
F 3 "" H 14225 5050 50  0001 C CNN
	1    14225 5050
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0158
U 1 1 6756456C
P 14225 5150
F 0 "#PWR0158" H 14225 5000 50  0001 C CNN
F 1 "+3V3" V 14240 5278 50  0000 L CNN
F 2 "" H 14225 5150 50  0001 C CNN
F 3 "" H 14225 5150 50  0001 C CNN
	1    14225 5150
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0159
U 1 1 67689CF0
P 14225 2075
F 0 "#PWR0159" H 14225 1925 50  0001 C CNN
F 1 "+5V" V 14240 2203 50  0000 L CNN
F 2 "" H 14225 2075 50  0001 C CNN
F 3 "" H 14225 2075 50  0001 C CNN
	1    14225 2075
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0160
U 1 1 67689CF6
P 14225 2175
F 0 "#PWR0160" H 14225 2025 50  0001 C CNN
F 1 "+3V3" V 14240 2303 50  0000 L CNN
F 2 "" H 14225 2175 50  0001 C CNN
F 3 "" H 14225 2175 50  0001 C CNN
	1    14225 2175
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0161
U 1 1 677AF7D8
P 10575 2075
F 0 "#PWR0161" H 10575 1925 50  0001 C CNN
F 1 "+5V" V 10590 2203 50  0000 L CNN
F 2 "" H 10575 2075 50  0001 C CNN
F 3 "" H 10575 2075 50  0001 C CNN
	1    10575 2075
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0162
U 1 1 677AF7DE
P 10575 2175
F 0 "#PWR0162" H 10575 2025 50  0001 C CNN
F 1 "+3V3" V 10590 2303 50  0000 L CNN
F 2 "" H 10575 2175 50  0001 C CNN
F 3 "" H 10575 2175 50  0001 C CNN
	1    10575 2175
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0163
U 1 1 678D612A
P 7150 2075
F 0 "#PWR0163" H 7150 1925 50  0001 C CNN
F 1 "+5V" V 7165 2203 50  0000 L CNN
F 2 "" H 7150 2075 50  0001 C CNN
F 3 "" H 7150 2075 50  0001 C CNN
	1    7150 2075
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0164
U 1 1 678D6130
P 7150 2175
F 0 "#PWR0164" H 7150 2025 50  0001 C CNN
F 1 "+3V3" V 7165 2303 50  0000 L CNN
F 2 "" H 7150 2175 50  0001 C CNN
F 3 "" H 7150 2175 50  0001 C CNN
	1    7150 2175
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0165
U 1 1 5F8A509D
P 1000 1350
F 0 "#PWR0165" H 1000 1100 50  0001 C CNN
F 1 "GND" H 1005 1177 50  0000 C CNN
F 2 "" H 1000 1350 50  0001 C CNN
F 3 "" H 1000 1350 50  0001 C CNN
	1    1000 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	875  1075 1000 1075
Wire Wire Line
	875  1125 1000 1125
Wire Wire Line
	1000 1075 1000 1125
Connection ~ 1000 1075
Wire Wire Line
	1000 1075 1125 1075
Wire Wire Line
	1000 1125 1125 1125
Wire Wire Line
	4300 1075 4425 1075
Wire Wire Line
	4300 1125 4425 1125
Wire Wire Line
	4425 1075 4425 1125
Connection ~ 4425 1075
Wire Wire Line
	4425 1075 4550 1075
Wire Wire Line
	4425 1125 4550 1125
Wire Wire Line
	7725 1075 7850 1075
Connection ~ 7850 1075
Wire Wire Line
	7850 1075 7975 1075
Wire Wire Line
	875  4050 1000 4050
Wire Wire Line
	875  4100 1000 4100
Wire Wire Line
	1000 4050 1000 4100
Connection ~ 1000 4050
Wire Wire Line
	1000 4050 1125 4050
Wire Wire Line
	1000 4100 1125 4100
Wire Wire Line
	4300 4050 4425 4050
Wire Wire Line
	4300 4100 4425 4100
Wire Wire Line
	4425 4050 4425 4100
Connection ~ 4425 4050
Wire Wire Line
	4425 4050 4550 4050
Wire Wire Line
	4425 4100 4550 4100
Wire Wire Line
	7725 4050 7850 4050
Wire Wire Line
	7725 4100 7850 4100
Wire Wire Line
	7850 4050 7850 4100
Connection ~ 7850 4050
Wire Wire Line
	7850 4050 7975 4050
Wire Wire Line
	7850 4100 7975 4100
Wire Wire Line
	975  6925 1225 6925
Connection ~ 1775 6925
Wire Wire Line
	1775 6925 2075 6925
Connection ~ 2075 6925
Wire Wire Line
	2075 6925 2325 6925
Wire Wire Line
	975  7125 1225 7125
Connection ~ 1225 7125
Wire Wire Line
	1225 7125 1475 7125
Connection ~ 2075 7125
Wire Wire Line
	2075 7125 2325 7125
Wire Wire Line
	1650 6925 1650 7000
Wire Wire Line
	1650 6925 1775 6925
Connection ~ 1650 7125
$Comp
L power:GND #PWR0166
U 1 1 6157BB0F
P 1650 7200
F 0 "#PWR0166" H 1650 6950 50  0001 C CNN
F 1 "GND" H 1655 7027 50  0000 C CNN
F 2 "" H 1650 7200 50  0001 C CNN
F 3 "" H 1650 7200 50  0001 C CNN
	1    1650 7200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0167
U 1 1 61584C01
P 1000 4350
F 0 "#PWR0167" H 1000 4100 50  0001 C CNN
F 1 "GND" H 1005 4177 50  0000 C CNN
F 2 "" H 1000 4350 50  0001 C CNN
F 3 "" H 1000 4350 50  0001 C CNN
	1    1000 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0168
U 1 1 61588D33
P 4425 4350
F 0 "#PWR0168" H 4425 4100 50  0001 C CNN
F 1 "GND" H 4430 4177 50  0000 C CNN
F 2 "" H 4425 4350 50  0001 C CNN
F 3 "" H 4425 4350 50  0001 C CNN
	1    4425 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0169
U 1 1 615906F8
P 7850 4350
F 0 "#PWR0169" H 7850 4100 50  0001 C CNN
F 1 "GND" H 7855 4177 50  0000 C CNN
F 2 "" H 7850 4350 50  0001 C CNN
F 3 "" H 7850 4350 50  0001 C CNN
	1    7850 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0170
U 1 1 61594451
P 7850 1375
F 0 "#PWR0170" H 7850 1125 50  0001 C CNN
F 1 "GND" H 7855 1202 50  0000 C CNN
F 2 "" H 7850 1375 50  0001 C CNN
F 3 "" H 7850 1375 50  0001 C CNN
	1    7850 1375
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0171
U 1 1 61597D7E
P 4425 1375
F 0 "#PWR0171" H 4425 1125 50  0001 C CNN
F 1 "GND" H 4430 1202 50  0000 C CNN
F 2 "" H 4425 1375 50  0001 C CNN
F 3 "" H 4425 1375 50  0001 C CNN
	1    4425 1375
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 7200 1650 7125
NoConn ~ 1525 6925
Wire Wire Line
	1225 6925 1325 6925
Wire Wire Line
	1325 6925 1325 7000
Wire Wire Line
	1325 7000 1650 7000
Connection ~ 1225 6925
Connection ~ 1650 7000
Wire Wire Line
	1650 7000 1650 7125
Wire Wire Line
	3075 4250 3150 4250
Wire Wire Line
	3075 4150 3150 4150
Wire Wire Line
	3075 6350 3150 6350
Wire Wire Line
	3075 4050 3150 4050
Wire Wire Line
	3075 6250 3150 6250
Wire Wire Line
	3075 6150 3150 6150
Wire Wire Line
	3075 6050 3150 6050
Wire Wire Line
	3075 5950 3150 5950
Wire Wire Line
	3075 5850 3150 5850
Wire Wire Line
	3075 5750 3150 5750
Wire Wire Line
	3075 5650 3150 5650
Wire Wire Line
	3075 5550 3150 5550
Wire Wire Line
	3075 5450 3150 5450
Wire Wire Line
	3075 5350 3150 5350
Wire Wire Line
	3075 3950 3150 3950
Wire Wire Line
	3075 5250 3150 5250
Wire Wire Line
	3075 5150 3150 5150
Wire Wire Line
	3075 5050 3150 5050
Wire Wire Line
	3075 4950 3150 4950
Wire Wire Line
	3075 4850 3150 4850
Wire Wire Line
	3075 4750 3150 4750
Wire Wire Line
	3075 4650 3150 4650
Wire Wire Line
	3075 4550 3150 4550
Wire Wire Line
	3075 4450 3150 4450
Wire Wire Line
	3075 4350 3150 4350
Wire Wire Line
	3075 3850 3150 3850
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2_4
U 1 1 5FA03185
P 3350 5050
F 0 "H2_4" H 3400 6475 60  0000 C CNN
F 1 "HEADER_2x26" H 3400 6375 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 3350 6441 60  0001 C CNN
F 3 "" H 3350 6300 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 3350 5050 50  0001 C CNN "MFPN"
	1    3350 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1_4
U 1 1 5FA0317F
P 2000 5050
F 0 "H1_4" H 2050 6475 60  0000 C CNN
F 1 "HEADER_2x26" H 2050 6375 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 2000 6441 60  0001 C CNN
F 3 "" H 2000 6300 60  0000 C CNN
F 4 "Samtec ESW-126-12-T-D" H 2000 5050 50  0001 C CNN "MFPN"
	1    2000 5050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M4_1
U 1 1 5F7D0737
P 1125 1225
F 0 "M4_1" H 950 1425 60  0000 L CNN
F 1 "HOLE" V 1197 1280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1125 1225 60  0001 C CNN
F 3 "" H 1125 1225 60  0000 C CNN
	1    1125 1225
	-1   0    0    1   
$EndComp
Connection ~ 1000 1125
Wire Wire Line
	1000 1125 1000 1350
Connection ~ 1475 7125
Wire Wire Line
	1475 7125 1650 7125
Connection ~ 1850 7125
Wire Wire Line
	1850 7125 2075 7125
Wire Wire Line
	1650 7125 1850 7125
Connection ~ 4425 1125
Wire Wire Line
	4425 1125 4425 1375
$Comp
L Mechanical:MountingHole_Pad M2_1
U 1 1 5F7CF7DD
P 1125 975
F 0 "M2_1" H 1025 1150 60  0000 L CNN
F 1 "HOLE" V 1197 1030 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1125 975 60  0001 C CNN
F 3 "" H 1125 975 60  0000 C CNN
	1    1125 975 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M4_2
U 1 1 5F9B96F1
P 4550 1225
F 0 "M4_2" H 4400 1400 60  0000 L CNN
F 1 "HOLE" V 4622 1280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 4550 1225 60  0001 C CNN
F 3 "" H 4550 1225 60  0000 C CNN
	1    4550 1225
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M1_3
U 1 1 5F9D4541
P 7725 975
F 0 "M1_3" H 7600 1150 60  0000 L CNN
F 1 "HOLE" V 7797 1030 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7725 975 60  0001 C CNN
F 3 "" H 7725 975 60  0000 C CNN
	1    7725 975 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M2_3
U 1 1 5F9D4547
P 7975 975
F 0 "M2_3" H 7875 1150 60  0000 L CNN
F 1 "HOLE" V 8047 1030 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7975 975 60  0001 C CNN
F 3 "" H 7975 975 60  0000 C CNN
	1    7975 975 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 1125 7975 1125
Wire Wire Line
	7850 1075 7850 1125
Connection ~ 7850 1125
$Comp
L Mechanical:MountingHole_Pad M3_3
U 1 1 5F9D454D
P 7725 1225
F 0 "M3_3" H 7675 1400 60  0000 L CNN
F 1 "HOLE" V 7797 1280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7725 1225 60  0001 C CNN
F 3 "" H 7725 1225 60  0000 C CNN
	1    7725 1225
	-1   0    0    1   
$EndComp
Wire Wire Line
	7725 1125 7850 1125
Wire Wire Line
	7850 1125 7850 1375
$Comp
L Mechanical:MountingHole_Pad M4_3
U 1 1 5F9D4553
P 7975 1225
F 0 "M4_3" H 7825 1400 60  0000 L CNN
F 1 "HOLE" V 8047 1280 60  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7975 1225 60  0001 C CNN
F 3 "" H 7975 1225 60  0000 C CNN
	1    7975 1225
	-1   0    0    1   
$EndComp
Connection ~ 1000 4100
Wire Wire Line
	1000 4100 1000 4350
Connection ~ 4425 4100
Wire Wire Line
	4425 4100 4425 4350
Connection ~ 7850 4100
Wire Wire Line
	7850 4100 7850 4350
$EndSCHEMATC
